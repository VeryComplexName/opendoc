<#
.SYNOPSIS
Wrapper for Office2Md\Office.ps1 (see README.md for details)

.DESCRIPTION
It intended for hide all internals from enduser for provide sinple interface tot he Classes for documents convertion

.EXAMPLE
.\Office2Md.ps1 Text.docx c:\Markdown
c:\docx\Dcx2Md.ps1 -DocxPath c:\docx\Test.docx -MdDir c:\temp\md

.NOTES
Version    : 0.11
Author     : Alexander Ivanov
Email      : Alexander.Ivan0v@outlook.com
Created on : 2021-12-06 11:01:06 UTC
#>

param(
    [parameter(Mandatory=$true, position=0)] [string[]] $DocPath,
    [parameter(Mandatory=$true, position=1)] [string] $MdDir,
    [parameter(Mandatory=$false, position=2)] [switch] $GithubFlawor,
    [parameter(Mandatory=$false, position=3)] [switch] $IntoSingleOne,
    [parameter(Mandatory=$false, position=4)] [switch] $NoPrereqCheck
)

$scriptPath = Split-Path -parent $MyInvocation.MyCommand.Definition
$logFile = Join-Path -Path $scriptPath -ChildPath $([System.IO.Path]::GetFileNameWithoutExtension($MyInvocation.MyCommand.Definition) + ".log")
$cfgFile = Join-Path -Path $scriptPath -ChildPath $([System.IO.Path]::GetFileNameWithoutExtension($MyInvocation.MyCommand.Definition) + ".json")
$fileName = [System.IO.Path]::GetFileNameWithoutExtension($MyInvocation.MyCommand.Definition)
$commonModule = Join-Path (Join-Path $scriptPath $fileName) "O2mCommon.psm1"
$classesPath =Join-Path (Join-Path $scriptPath $fileName) "Office.ps1"
$pdfPath =Join-Path (Join-Path $scriptPath $fileName) "Pdf.ps1"
$castle =Join-Path (Join-Path $scriptPath $fileName) "BouncyCastle.Crypto.dll"
$rc = 0;

if(!(Test-Path $commonModule)) {"Error: there is no required module [$commonModule]"; exit 1;}
Import-Module $commonModule  -PassThru -DisableNameChecking -Global | Out-Null

$info = Get-PowerShellFileInfo $MyInvocation.MyCommand.Definition
toLog "Office2Md v$($info.Version) Copyright (c) $($info.Author) [$($info.Email)]"
$psPlatform = Get-PSPlatform

if(!$NoPrereqCheck.IsPresent) {
    toLog 'Downloading/Installing or Checking prerequisities and loading libraries...'
    Install-PreReq
}
else {toLog "Skipping prerequisities check"}

$reqPackages = @(
    @{Name = 'Open-XML-SDK'; MinVer = '2.9.1'; Dll = @(
        @{'Unix' = 'lib\netstandard1.3\DocumentFormat.OpenXml.dll'},
        @{'Windows.Core' = 'lib\netstandard1.3\DocumentFormat.OpenXml.dll'},
        @{'Windows' = 'lib\net46\DocumentFormat.OpenXml.dll'}
    )}
    #@{Name = 'Common.Logging.Core'; MinVer = '3.4.1'; Dll = @("lib\net40\Common.Logging.Core.dll")}
    #@{Name = 'Common.Logging'; MinVer = '3.4.1'; Dll = @('lib\net40\Common.Logging.dll')}
    #@{Name = 'BouncyCastle.Crypto.dll'; MinVer = '1.8.1'; Dll = 'lib\BouncyCastle.Crypto.dll'}
    #@{Name = 'iText7'; MinVer = '7.1.15'; Dll = @("lib\net45\itext.kernel.dll", 'lib\net45\itext.io.dll', 'lib\net45\itext.forms.dll', 'lib\net45\itext.layout.dll')}
    # This is why we have to have this dll already downloaded (Office2Md\BouncyCastle.Crypto.dll v1.8.10)       
    @{Name = 'iTextSharp'; MinVer = '5.5.13.2'; Dll = @(
        @{'Unix' = $null},
        @{'Windows.Core' = 'lib\itextsharp.dll'},
        @{'Windows' = 'lib\itextsharp.dll'}
    )}    
)

# Skip required DLLs loding if they are already loaded
if($null -eq $global:Office2MdRequiredDllsAlreadyLoaded -or $global:Office2MdRequiredDllsAlreadyLoaded -eq $false) {
    # ForiTextSharp in order to BouncyCastle.Crypto.dll will be available from Path
    $env:Path = "$(Join-Path $scriptPath $fileName);" + $env:Path

    # iTextSharp depend on BouncyCastle.Crypto. But at the moment Nuget.org has old .dll (1.8.1) but we must have (1.8.6.0) - Requires for PDF library itextsharp.dll
    if($psPlatform.StartsWith('Windows')) {Add-Type -Path $castle}

    foreach($p in $reqPackages) {
        toLog "[$($p.Name)] package..."
        if(!$p.SkipGetPackage) {Install-ThisPackage -Name $p.Name -MinimumVersion $p.MinVer}
        else {Install-Package -Name $p.Name -Scope CurrentUser -Force -Confirm:$false | Out-Null}
        $Packageinfo = Get-Package $p.Name
        foreach($dll in $p.Dll.$psPlatform) {
            if(![string]::IsNullOrEmpty($dll)) {Add-Type -Path (Join-Path (Split-Path $PackageInfo.Source) $dll) | Out-Null}
        }
    }
    toLog 'Done'
    $global:Office2MdRequiredDllsAlreadyLoaded = $true;
}

# Office support
if(!(Test-Path $classesPath)) {toLog "Error: there is no required file [$classesPath]" 'error'; exit 1;}
. $classesPath

# Pdf support
if($psPlatform -eq 'Windows') {
    . $pdfPath
}
else {toLog "There is no .PDF support on [$psPlatform]" 'warning'}

# Load all plugins
if($psPlatform -eq 'Windows') {
    $allPlugins = Load-Plugins (Join-Path (Join-Path $scriptPath ([System.IO.Path]::GetFileNameWithoutExtension($MyInvocation.MyCommand.Definition))) 'Plugins')
}
else {toLog "There is no any Plugins support on [$psPlatform] yet" 'warning'}

try {
    $all = ''
    $docFileName = [System.IO.Path]::GetFileNameWithoutExtension($DocPath[0])
    foreach($d in $DocPath) {
        switch([System.IO.Path]::GetExtension($d)) {
            '.pdf' {
                # Pdf
                if($psPlatform -eq 'Windows') {
                    $doc = [PdfItem]::new($d, $MdDir, $cfgFile, $allPlugins)
                }
                else {toLog "PDF file [$d] will not be processed" 'warning'}
                break;
            }
            default {
                # Office
                $doc = [DocumentItem]::new($d, $MdDir, $cfgFile, $allPlugins)
                break;
            }
        }
        if($null -ne $doc) {
            $doc.ProcessDocument();
            $doc.CloseDocument();
            $all += $doc.ToMarkdown($GithubFlawor.IsPresent) 
            if($IntoSingleOne.IsPresent) {$all += "`r`n`r`n-----`r`n`r`n"}
            else {
                $doc.SaveToMarkdown((Join-Path $MdDir "$([System.IO.Path]::GetFileNameWithoutExtension($d)).md"), $GithubFlawor.IsPresent)
                #$doc.SaveToCsv($MdDir)
            }

            #$doc.SaveToMarkdown((Join-Path $MdDir "$([System.IO.Path]::GetFileNameWithoutExtension($d)).md"), $GithubFlawor.IsPresent)
            #$doc.SaveToMarkdownFor11ty((Join-Path $MdDir '11ty'), $GithubFlawor.IsPresent)
            #$doc.SaveToText((Join-Path $MdDir "$([System.IO.Path]::GetFileNameWithoutExtension($d)).txt"))
        }
        else {toLog "Document [$d] wasn't converted" 'warning'}
    }
    if(![string]::IsNullOrEmpty($all)) {if($IntoSingleOne.IsPresent) {$all | Out-File (Join-Path $MdDir "$docFileName.md") -Encoding utf8}}
}
catch {
    $rc = 1;
    toLog ($_ | Out-String) error
}
finally{
    if($null -ne $doc) {
        if($null -ne $doc.Stream) {$doc.Stream.Dispose();}
        if($null -ne $doc.Document) {$doc.Document.Dispose()}
    }
    $mName = [System.IO.Path]::GetFileNameWithoutExtension($MyInvocation.MyCommand.Definition)
    if($null -ne (Get-Module $mName)) {Remove-Module $mName}
}

exit $rc;