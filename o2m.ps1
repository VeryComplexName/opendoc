Function OpenFile($initialDirectory)
{ 
    [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null

    $OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
    $OpenFileDialog.initialDirectory = $initialDirectory
    $OpenFileDialog.Multiselect = $true;
    $OpenFileDialog.ShowHelp = $true
    $OpenFileDialog.filter = "All Supported Files (*.docx, *.xlsx, *.pptx, *.pdf)|*.docx;*.xlsx;*.pptx;*.pdf"
    $result = $OpenFileDialog.ShowDialog()
    if($result = 'Ok') {
        $OpenFileDialog.FileNames
    }
}

$SelectOnClick = {
    $fb = New-Object System.Windows.Forms.FolderBrowserDialog
    $ret = $fb.ShowDialog()
    if($ret -eq 'OK') {
        $txtDest.Text = $fb.SelectedPath
    }
}

$AddOnClick = {
    $files = OpenFile('c:\')
    foreach($f in $files) {
        $lvSource.Items.Add((New-Object System.Windows.Forms.ListViewItem($f)))
    }
}

$delOnClick = {
    foreach($i in $lvSource.SelectedItems) {
        $lvSource.Items.Remove($i)
    }
}

function Check-AllInputs {
    $ret = $true;
    if($lvSource.Items.Count -le 0) {
        [System.Windows.MessageBox]::Show('You must choose source files'); $ret = $false
    }
    if([string]::IsNullOrEmpty($txtDest.Text)) {
        [System.Windows.MessageBox]::Show('You must select a destination folder'); $ret = $false
    }
    $ret
}

function GetLogContent {
    $lbLog.Text = ''
    try {
        (Get-Content $office2mdLog -ErrorAction SilentlyContinue) -split "`r" | %{$lbLog.Text += $_ + "`r`n"}
    } catch {}
}

$goOnClick = {
    if(Check-AllInputs) {
        $src = @()
        foreach($itm in $lvSource.Items) {
            $src += $itm.Text
        }

        $intoSingleOne = $null
        if($cbAllToOne.Checked) {
            $intoSingleOne = '-IntoSingleOne'
        }
        
        $run = {
            param(
                [string]$script,
                [string[]]$from,
                [string]$to,
                [bool]$single
            )

            if($single) {. $script $from $to -GithubFlawor $single}
            else {. $script $from $to -GithubFlawor}
        }

        $Job = $null
        try {
            $job = Start-Job $run -ArgumentList $office2md, $src, $txtDest.Text

            $State = ''
            while($State -ne 'Completed') {
                $State = (Get-Job $job.Id).jobstateinfo.state
                Sleep 3
                GetLogContent
            }
            (Receive-Job $job.Id) -split "`r" | %{$lbLog.Text += $_ + "`r`n"}
        }
        finally {
            if($null -ne $Job) {Remove-Job $job.Id}
        }

        GetLogContent

        if($cbOpenDest.Checked) { Invoke-Item $txtDest.Text}

        [System.Windows.MessageBox]::Show('Done')
    }
}


function Create-GUI {
    param(
        $obj, $left, $top, $width, $height, $parent, $text
    )

    $ret = New-Object $obj
    $ret.Left = $left; $ret.Top = $top;
    $ret.Width = $width; $ret.Height = $height;
    if(![string]::IsNullOrEmpty($text)) {$ret.Text = $text}
    if($null -ne $parent) {$parent.Controls.Add($ret)}

    $ret
}

function Create-MainForm {
    param (
        $info
    )

    $mainForm = New-Object System.Windows.Forms.Form
    $mainForm.Text = "GUI for Office2Md"
    $mainForm.Width = 705
    $mainForm.Height = 489
    $mainForm.AutoSize = $true
    #$mianForm.StartPosition = [System.Windows.Forms.FormStartPosition]::CenterScreen

    # Files to convert
    $gbSource = Create-GUI System.Windows.Forms.GroupBox 12 11 336 200 $mainForm 'Source Files'
    $btnAdd = Create-GUI System.Windows.Forms.Button 18 21 75 23 $gbSource 'Add'
    $btnAdd.Add_Click($AddOnClick)
    $btnDel = Create-GUI System.Windows.Forms.Button 242 21 75 23 $gbSource 'Remove'
    $btnDel.Add_Click($delOnClick)
    $lvSource = Create-GUI System.Windows.Forms.ListView 15 50 302 134 $gbSource ''
    $lvSource.View = 'List';

    $gbDest = Create-GUI System.Windows.Forms.GroupBox 354 12 321 87 $mainForm 'Destination'
    $txtDest = Create-GUI System.Windows.Forms.TextBox 6 19 211 20 $gbDest ''
    $btnSelect = Create-GUI System.Windows.Forms.Button 233 16 75 23 $gbDest 'Select'
    $btnSelect.Add_Click($SelectOnClick)
    $cbAllToOne = Create-GUI System.Windows.Forms.CheckBox 7 49 72 17 $gbDest 'All to One'
    $cbOpenDest = Create-GUI System.Windows.Forms.CheckBox 99 49 219 17 $gbDest 'Open destination Folder on completion'

    $gbLog = Create-GUI System.Windows.Forms.GroupBox 12 217 663 221 $mainForm 'Log'
    $lbLog = Create-GUI System.Windows.Forms.TextBox 16 19 634 186 $gbLog ''
    $lbLog.ReadOnly = $true
    $lbLog.Multiline = $true
    $lbLog.ScrollBars = 'Both'

    $btnGo = Create-GUI System.Windows.Forms.Button 354 105 321 106 $mainForm 'Go! Go! Go!'
    $fnt = New-Object System.Drawing.Font($btnGo.Font.Name, 15, $btnGo.Font.Style)
    $btnGo.Font = $fnt
    $btnGo.Add_Click($goOnClick)

    $result = $mainForm.ShowDialog()

    $mainForm.Dispose()
    $mainForm = $null
}

if($PSVersionTable.PSEdition -ne 'Core') {
    $scriptPath = Split-Path -parent $MyInvocation.MyCommand.Definition
    $logFile = Join-Path -Path $scriptPath -ChildPath $([System.IO.Path]::GetFileNameWithoutExtension($MyInvocation.MyCommand.Definition) + ".log")
    $myLogFile = $logFile
    $o2mScriptName = 'Office2Md'
    $commonModule = Join-Path (Join-Path (Join-Path $scriptPath $o2mScriptName) $fileName) "O2mCommon.psm1"
    $office2md = Join-Path $scriptPath ($o2mScriptName + '.ps1')
    $office2mdLog = Join-Path $scriptPath ($o2mScriptName + '.log')

    if(!(Test-Path $commonModule)) {"Error: there is no required module [$commonModule]"; exit 1;}
    Import-Module $commonModule  -PassThru -DisableNameChecking -Global | Out-Null

    if(!(Test-Path $office2md)) {toLog "There is no required file [$office2md]"; exit 1;}
    try {
        del $myLogFile -Force -Confirm:$false -ErrorAction SilentlyContinue
        del $office2mdLog -Force -Confirm:$false -ErrorAction SilentlyContinue
    } catch {}

    $info = Get-PowerShellFileInfo $MyInvocation.MyCommand.Definition

    $mainForm = $null
    $btnAdd = $null
    $btnDel = $null
    $lvSource = $null
    $txtDest = $null
    $btnSelect = $null
    $lbLog = $null
    $btnGo = $null
    $cbAllToOne = $null
    $cbOpenDest = $null

    # ---------- Running in GUI -------------
    [void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")

    Create-MainForm $info
}
else {Write-Output "Won't work in PowerShell Core"}