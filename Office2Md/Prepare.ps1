<#
.SYNOPSIS
Prepare Git, Python, download [https://gitlab.com/VeryComplexName/opendoc] repository and run Office2Md GUI

.DESCRIPTION
It intended for install/check: 
- Git
- Python
- Office2Md (https://gitlab.com/VeryComplexName/opendoc)
- Also run GUI for Office2Md 
All above tasks could be done as separately as well as all of them. 
Script is independent and can be used for some other tasks like install Git, install Python for example

.EXAMPLE
.\Prepare.ps1 -Git -Python -Office2Md -Install -RunOffice2Md
.\Prepare.ps1 -Git -Python -Office2Md -Install
.\Prepare.ps1 -Git -Python -Office2Md
.\Prepare.ps1 -Git -Python -Check

.NOTES
Version    : 0.0.1
Author     : Alexander Ivanov
Email      : Alexander.Ivan0v@outlook.com
Created    : 2021-12-13 15:12:01 UTC
#>

[CmdletBinding(DefaultParameterSetName='main')]
param(
    [parameter(ParameterSetName='main', Mandatory=$false)] [string] $Install,
    [parameter(ParameterSetName='main', Mandatory=$false)] [switch] $Office2Md,
    [parameter(ParameterSetName='main', Mandatory=$false)] [switch] $RunOffice2Md,    

    [parameter(ParameterSetName='check', Mandatory=$false)] [switch] $Check,

    [parameter(Mandatory=$false)] [switch] $Git,
    [parameter(Mandatory=$false)] [switch] $Python
    
)

function GetCurrentFunctionName {
    (Get-PSCallStack | Select -First 2 | Select -Last 1).Command
}

function Get-PSPlatform {
    if($null -ne $PSVersionTable.Platform) {
        if($PSVersionTable.Platform.StartsWith('Win')) {return 'Windows.Core'}
        elseif($PSVersionTable.Platform.StartsWith('Unix')) {return 'Unix'}
        else {return 'Unknown'}
    }
    else {return 'Windows'}
}

function Parse-Notes {
    param(
        [string]$Notes
    )

    $ret = @{}
    $str = $Notes -split "`n"
    if($null -ne $str -and $str.Count -ge 1) {
        foreach($s in $str) {
            if(![string]::IsNullOrEmpty($s)) {
                $tmp = $s.Split(':')
                if($tmp.Count -ge 2) {
                    for($i=0; $i -lt $tmp.Count; $i++) { $tmp[$i] = $tmp[$i].Trim()<#.ToLower();#> }
                    if($tmp.Count -eq 2) {$ret += @{$tmp[0] = $tmp[1]}}
                    else {
                        $tmp1 = $tmp | ? {$_ -ne $tmp[0]}
                        $ret += @{$tmp[0] = $tmp1 -join ':'}
                    }
                }
            }
        }
    }
    $ret
}

function Get-PowerShellFileInfo {
    param(
        [string]$PathToPs1OrPsm1
    )
    
    $ret = $null
    if((Test-Path $PathToPs1OrPsm1 -PathType Leaf)) {
        $astTokens = $null; $astErr = $null; $funcList = @();
        $ast = [System.Management.Automation.Language.Parser]::ParseFile($PathToPs1OrPsm1, [ref]$astTokens, [ref]$astErr)
        $ret = Parse-Notes ($ast.GetHelpContent().Notes)
        $ret += @{'Synopsis' = $ast.GetHelpContent().Synopsis.Trim()}
        $ret += @{'Description' = $ast.GetHelpContent().Description.Trim()}
    }
    $ret
}

function toLog {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingWriteHost', '', Scope='Function')]
    [CmdletBinding(SupportsShouldProcess=$true, ConfirmImpact="Low")]
    param(
        [parameter(Mandatory=$true, position=0)][ValidateNotNullOrEmpty()]$msg,
        [parameter(Mandatory=$false, position=1)][ValidateSet('info', 'error', 'warning')][string]$what = "info"
    )
    $dat = Get-Date;
    $datStr = $dat.ToString('yyyy-MM-dd HH:mm:ss')
    switch($what) {
        'info' {
            Write-Host "[$datStr] $msg";
            break;
        }
        'error' {
            Write-Host "[$datStr] $msg" -ForegroundColor Red;
            break;
        }
         'warning' {
            Write-Host "[$datStr] $msg" -ForegroundColor Yellow;
            break;
        }
    }    
    # Log to the file
    try {
        if($null -ne $global:logFile) {
            $parent = Split-Path $global:logFile
            if(!(test-Path $parent)) {mkdir $parent}
            "[$datStr] [$($what.ToUpper())] $msg" >> $global:logFile
        }
    }
    catch {
        Write-Host "Can't write to log. [$datStr] $msg" -ForegroundColor Red;
    }
}

# ---------------- Main program starts --------------
$scriptPath = Split-Path $MyInvocation.MyCommand.Definition

$info = Get-PowerShellFileInfo $MyInvocation.MyCommand.Definition
toLog "$($info.Synopsis) Author: $($info.Author) Version: $($info.Version)"

