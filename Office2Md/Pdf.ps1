class PdfItem {
    [System.Collections.ArrayList] $Pages
    [string]$Path
    [string]$MdDir 
    $cfg
    $AllPlugins
    $PdfReader
    $Pdf

    PdfItem([string]$Path, [string]$MdDir, [string]$CfgFile, [string[]]$AllPlugins) {
        $this.AllPlugins = $AllPlugins
        $this.Pages = [System.Collections.ArrayList]::new()

        if((Test-Path $CfgFile)) {$this.Cfg = Get-Content $CfgFile -Raw | ConvertFrom-Json}
        else {throw "There is no such configuration file [$CfgFile]"}

        if((Test-Path $Path)) {
            $this.Pdf = New-Object iTextSharp.text.pdf.pdfreader -ArgumentList $Path
            #$this.PdfReader = [iText.Kernel.pdf.pdfreader]::new($Path)
            #$this.Pdf = [iText.Kernel.Pdf.PdfDocument]::new($this.PdfReader)
            
        }
        else {throw "There is no such PDF file [$Path]"}
    }

    [void] ProcessDocument() {
        if($null -ne $this.Pdf) {
            for ($page = 1; $page -le $this.Pdf.NumberOfPages; $page++){

                <#
                for($i = 1; $i -le $this.Pdf.GetNumberOfPages(); $i++) {
                    $page = $this.Pdf.GetPage($i);
                }
                #>
                
                $this.Pages.Add(
                    @{ Page = $page;
                       Text = [iTextSharp.text.pdf.parser.PdfTextExtractor]::GetTextFromPage($this.Pdf,$page)
                    }
                )
                
            }
        }
    }

    [void] CloseDocument() {
        if($null -ne $this.Pdf) {
            $this.Pdf.Close()
            $this.Pdf = $null;
        }
    }

    [string] FooterOrHeader([int] $PageNum, [string] $Position) {
        $ret = '';

        if($this.cfg.Pdf.UsePageNumbers) {
            if($Position -eq $this.cfg.Pdf.Position) {
                $ret += "`r`n`r`n$($this.cfg.Pdf.Template.Replace('{PageNumber}', $PageNum))`r`n`r`n"
            }
        }
        return $ret;
    }

    [string] ToMarkdown([bool] $GithubFlawor) {
        [string] $ret = ''

        foreach($p in $this.Pages) {
            $ret += $this.FooterOrHeader($p.Page, 'header');
            $ret += $p.Text            
            $ret += $this.FooterOrHeader($p.Page, 'footer');
            
            # Replace or Escape special characters
            <#
            $ret = $ret.Replace('|', ':')
            $ret = $ret.Replace('`', '\`').
                Replace('*', '\*').
                Replace('_', '\_').
                Replace('#', '\#').
                Replace('+', '\+').
                Replace('-', '\-').
                Replace('.', '\.').
                Replace('!', '\!').
                Replace('(', '\(').
                Replace(')', '\)').
                Replace('{', '\{').
                Replace('}', '\}').
                Replace('[', '\[').
                Replace(']', '\]')
            #>
            # Replace wrong unicode characters
            foreach($rep in $this.Cfg.ReplaceThis) {
                $ret = $ret.Replace($rep.From, $rep.To)
            }
        }

        return $ret;
    }

    [void] SaveToMarkdown([string] $FileName, [bool] $GithubFlawor) {
        toLog "Saving [$($this.Path)] to [$FileName] as Markdown..."
        $this.ToMarkdown($GithubFlawor) | Out-File $FileName
        toLog "Saved to [$FileName]"
    }
}