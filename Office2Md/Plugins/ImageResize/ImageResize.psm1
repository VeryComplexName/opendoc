<#
.SYNOPSIS
Resize Images for [ImgItem] Class

.DESCRIPTION
Resize images using Kaliko Image Library (https://kaliko.com/). It's slight better than standard method in [ImgItem].
This module created just for demonstrating ability of plugins.
See README for the details.

.NOTES
Version    : 0.1
Author     : Alexander Ivanov
Created on : 2021-07-07 09:07:54 UTC
#>

# Main functionality
function ImgItem-BeforeConvertAndSave {
    param(
        [object[]]$arg
    )

    # $img, $stream, $savePath, $this.Document.Cfg.Image
    # Look on Ofice.ps1 (method ConvertAndSave for the details)
    return @($arg[0], $arg[1], $arg[2], $arg[3])
}

function InternalResize {
    param(
        [System.Drawing.Image] $image,
        $Width,
        $Heighht
    )

    $kImg = [Kaliko.ImageLibrary.KalikoImage]::new($image)
    $kImg.Resize($Width, $Height)
    $kImg.GetAsBitmap()
}

function ImgItem-ResizeImage {
    param(
        [object[]]$arg
    )

    # Look on Ofice.ps1 (method ConvertAndSave for the details)
    $image = $arg[0]; $imgCfg = $arg[1]

    if($imgCfg.Convert.Resize.UseResize) {
        # We must use just one of 2 dimension: MaxWidth or MaxHeight
        # MaxWidth is in priority
        $width = 100; $height = 100; $magnify = 1;

        if($imgCfg.Convert.Resize.MaxWidth -gt 0) {
            if($image.Width -gt $imgCfg.Convert.Resize.MaxWidth -or !$imgCfg.Convert.Resize.DoNotResizeLessThanMax) {
                $magnify = ($image.Width/$imgCfg.Convert.Resize.MaxWidth)
            }
        }
        elseif($imgCfg.Convert.Resize.MaxHeight) {
            if($image.Height -gt $imgCfg.Convert.Resize.MaxHeight -or !$imgCfg.Convert.Resize.DoNotResizeLessThanMax) {
                $magnify = ($image.Height/$imgCfg.Convert.Resize.MaxHeight)
            }
        }

        if($magnify -ne 1) {
            $width = [int]($image.Width / $magnify)
            $height = [int]($image.Height / $magnify)
            $image = InternalResize $image $width $height
        }
    }
    
    @($image, $imgCfg)
}

function ImgItem-Init {
    param(
        $path
    )

    # Will be called from O2mCommon.psm1 Module (Load-Plugins function)
    [System.Reflection.Assembly]::LoadFile((Join-Path $path 'Kaliko.ImageLibrary.dll')) | Out-Null
}

Export-ModuleMember ImgItem-*