<#
.SYNOPSIS
Simple module intended for provide common support for Office2Md.ps1 & Classes.ps1

.DESCRIPTION
It intended for simplify common things for: packages finding, loading, installing and logging

.NOTES
Version    : 0.1
Author     : Alexander Ivanov
Created on : 2021-05-18 09:07:54 UTC
#>

function toLog {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingWriteHost', '', Scope='Function')]
    [CmdletBinding(SupportsShouldProcess=$true, ConfirmImpact="Low")]
    param(
        [parameter(Mandatory=$true, position=0)][ValidateNotNullOrEmpty()]$msg,
        [parameter(Mandatory=$false, position=1)][ValidateSet('info', 'error', 'warning')][string]$what = "info"
    )
    $dat = Get-Date;
    $datStr = $dat.ToString('yyyy-MM-dd HH:mm:ss')
    switch($what) {
        'info' {
            Write-Host "[$datStr] $msg";
            break;
        }
        'error' {
            Write-Host "[$datStr] $msg" -ForegroundColor Red;
            break;
        }
         'warning' {
            Write-Host "[$datStr] $msg" -ForegroundColor Yellow;
            break;
        }
    }    
    # Log to the file
    try {
        if($null -ne $logFile) {
            $parent = Split-Path $logFile
            if(!(test-Path $parent)) {mkdir $parent}
            "[$datStr] [$($what.ToUpper())] $msg" >> $logFile
        }
    }
    catch {
        Write-Host "Can't write to log. [$datStr] $msg" -ForegroundColor Red;
    }
}

function Install-PreReq {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingCmdletAliases', '', Scope='Function')]
    
    [Version]$nugetVer = '2.8.5.201'

    $nugetProvider = Get-PackageProvider -List | Where-Object {$_.Name -eq 'NuGet'}
    if($nugetProvider.Count -gt 1) {$nugetProvider = $nugetProvider | Sort Version -Desc | Select -First 1}
    if($null -ne $nugetProvider -and $nugetProvider.Version -lt $nugetVer) {
        throw "We have NuGet version [$($nugetProvider.Version)] less than requested [$nugetVer]. So you must manually remove directory [$(Split-Path (Split-Path ($nugetProvider.ProviderPath)))] from computer first."
    }
    if($null -eq $nugetProvider -or $nugetProvider.Version -lt $nugetVer) {Install-PackageProvider -Name NuGet -Scope CurrentUser -MinimumVersion $nugetVer -Force}

    $nugetPackagesourceLocation = 'https://www.nuget.org/api/v2'
    if($null -eq (Get-Packagesource | ?{$_.Location -eq $nugetPackagesourceLocation})) {Register-PackageSource -Name nuget.org -Location $nugetPackagesourceLocation -ProviderName NuGet}

    $policy = Get-PSRepository -Name "PSGallery" | Select-Object -ExpandProperty "InstallationPolicy"
    if($policy -ne "Trusted") {Set-PSRepository -Name "PSGallery" -InstallationPolicy Trusted}
}

function Install-Dependency {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingEmptyCatchBlock', '', Scope='Function')]
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingCmdletAliases', '', Scope='Function')]

    param (
        [parameter(Mandatory = $true)][string] $Name,
        [parameter(Mandatory = $false)][Version]$MinimumVersion
    )

    $analyzer = Get-Module -Name $Name -ListAvailable
    if($null -ne $analyzer -and $analyzer.Count -gt 1 -or ($null -ne $MinimumVersion -and $analyzer.Version -lt $MinimumVersion)) {
        foreach( $a in $analyzer | Sort Version) {
            try {$a | Uninstall-Module -Force -Confirm:$false -ErrorAction SilentlyContinue} catch {}
        }
        $analyzer = Get-Module -Name $Name -ListAvailable
    }
    if ($null -eq $analyzer) {
        if($null -eq $MinimumVersion) {Install-Module -Name $Name -Scope CurrentUser -Force -SkipPublisherCheck}
        else {Install-Module -Name $Name -MinimumVersion $MinimumVersion -Scope CurrentUser -Force -SkipPublisherCheck}
    }    
}


function Install-ThisPackage {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingEmptyCatchBlock', '', Scope='Function')]
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingCmdletAliases', '', Scope='Function')]

    param (
        [parameter(Mandatory = $true)][string] $Name,
        [parameter(Mandatory = $false)][Version]$MinimumVersion
    )

    $analyzer = Get-Package -Name $Name -ErrorAction SilentlyContinue
    if ($null -eq $analyzer) {
        if($null -eq $MinimumVersion) {Install-Package -Name $Name -Scope CurrentUser -SkipDependencies -Force -Confirm:$false | Out-Null}
        else {Install-Package -Name $Name -MinimumVersion $MinimumVersion -Scope CurrentUser -SkipDependencies -Force -Confirm:$false | Out-Null}
    }    
}

function Parse-Notes {
    param(
        [string]$Notes
    )

    $ret = @{}
    $str = $Notes -split "`n"
    if($null -ne $str -and $str.Count -ge 1) {
        foreach($s in $str) {
            $tmp = $s.Split(':')
            if($tmp.Count -eq 2) {
                for($i=0; $i -lt $tmp.Count; $i++) { $tmp[$i] = $tmp[$i].Trim()<#.ToLower();#> }
                $ret += @{$tmp[0] = $tmp[1]}
            }
        }
    }
    $ret
}

function Get-PowerShellFileInfo {
    param(
        [string]$PathToPs1OrPsm1
    )
    
    if((Test-Path $PathToPs1OrPsm1)) {
        $astTokens = $null; $astErr = $null; $funcList = @();
        $ast = [System.Management.Automation.Language.Parser]::ParseFile($PathToPs1OrPsm1, [ref]$astTokens, [ref]$astErr)
        Parse-Notes ($ast.GetHelpContent().Notes)
    }
}

function Load-Plugins {
    param(
        [string]$PluginsPath
    )

    $ret = @()
    
    if((Test-Path $pluginsPath)) {
        [object[]]$pluginDirs = Get-ChildItem $pluginsPath | ?{ $_.PSIsContainer } | Select-Object FullName
        if($pluginDirs.Count -gt 0) {
            foreach($p in $pluginDirs) {
                [object[]]$module = dir (Join-Path $p.FullName '*.psm1') | Select FullName
                if($module.Count -eq 1) {
                    toLog "Found Plugin [$([System.IO.Path]::GetFileNameWithoutExtension($module.FullName))]"
                    $info = Get-PowerShellFileInfo $module.FullName
                    toLog "Author: $($info.Author) Version: $($info.Version)"

                    Import-Module $module.FullName -PassThru -DisableNameChecking -Global -Prefix "_$([System.IO.Path]::GetFileNameWithoutExtension($p.FullName))_" | Out-Null
                    $ret += $module.FullName
                    # Init module if required
                    if($null -ne (Get-Command "ImgItem-_$([System.IO.Path]::GetFileNameWithoutExtension($p.FullName))_Init")) {. "ImgItem-_$([System.IO.Path]::GetFileNameWithoutExtension($p.FullName))_Init" (Split-Path $module.FullName)}
                }
                else {toLog "There is should be just a single module for each plugin [$($module.FullName)]" error}
            }
        }
    }    

    $ret;
}

function GetCurrentFunctionName {
    (Get-PSCallStack | Select -First 2 | Select -Last 1).Command
}

function Get-PSPlatform {
    if($null -ne $PSVersionTable.Platform) {
        if($PSVersionTable.Platform.StartsWith('Win')) {return 'Windows.Core'}
        elseif($PSVersionTable.Platform.StartsWith('Unix')) {return 'Unix'}
        else {return 'Unknown'}
    }
    else {return 'Windows'}
}