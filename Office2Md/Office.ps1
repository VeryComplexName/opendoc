<#
.SYNOPSIS
Read and convert Microsoft Office documents to Markdowwn ones

.DESCRIPTION
This classes intended to work with:
docx|dotm|dotx
xlsx|xlsm|xltx|xltm
pptx|pptm|potx|potm|ppsx
Microsoft documents and allows to convert them to Markdown with Github flawor and other features.
Also it is possible to Save/Restore prepared classes to/from xml files in order to simplify
and speed up further convertion processes.

.EXAMPLE
$doc = [DocumentItem]::new($DocxPath, $MdDir, $cfgFile)
$doc.ProcessDocument();
$doc.CloseDocument();
$doc.SaveToMarkdown($FileName, $true)

.NOTES
Version    : 0.11
Author     : Alexander Ivanov
Created on : 2021-12-06 11:01:06 UTC
#>

function GetDateFormatIdKeys
{
    param ()

    $DateFormatId.Keys
}

function GetDateFormatId 
{
    param()
    
    $DateFormatId[$idx]
}

# ------- Especially for Excel start ---------
class Cell : System.Collections.ArrayList {
    $Comment

    Cell() : base() {
        $this.Comment = 'Fake Cell'
    }

    [void] SetPara([Paragraph]$Para) {
        $this.Add($Para);
    }
}

class Row : System.Collections.ArrayList {
    $Comment

    Row() : base() {
        $this.Comment = 'Fake Row'
    }
}

class Table : System.Collections.ArrayList {
    $Comment

    Table() : base() {
        $this.Comment = 'Fake Table'
    }
}

class Paragraph : System.Collections.ArrayList {
    $Comment

    Paragraph() : base() {
        $this.Comment = 'Fake Paragraph'
    }

    [Paragraph] SetRuns([DocumentFormat.OpenXml.OpenXmlCompositeElement[]]$Runs) {
        $Runs | % {$this.Add($_)}
        return $this;
    }

    [Paragraph] SetText([string]$Text) {
        $this.Add([Run]::new($Text))
        return $this;
    }
}

class Run : System.Collections.ArrayList {
    $Comment

    Run() : base() {
        $this.Comment = 'Fake Run'
        $this.Add([Text]::new())
    }

    Run($Text) : base() {
        $this.Comment = 'Fake Run'
        $this.Add([Text]::new($Text))
    }

    [void] SetText([string]$Text) {
        $txt = $this | ? {$_.GetType().Name -eq 'Text'}
        if($null -ne $txt) {$txt.SetText($Text)}
    }

    [string] GetText() {
        $text = $this | ? {$_.GetType().Name -eq 'Text'}
        if($null -ne $text) {$text.GetText($Text)}
        return $null
    }
}

class Text {
    $Text

    Text() : base() {

    }

    Text([string]$Text) : base() {
        $this.SetText($Text)
    }

    [void] SetText([string]$Text) {
        $this.Text = $Text
    }

    [string] GetText() {
        return $this.Text
    }
}

# ------- Especially for Excel end -----------

class BaseItem {    
    [hashtable] $Properties
    [guid] $Id
    [DocumentItem] $Document;
    [BaseItem] $Next;
    [BaseItem] $Prev;
    [BaseItem[]] $Children;
    [BaseItem] $Parent
    
    BaseItem([DocumentItem] $Document, [BaseItem] $Parent) {
        $this.Id = [guid]::NewGuid()
        $this.Document = $Document;
        $this.Parent = $Parent
    }

    [BaseItem] FirstChild() {
        if($this.Children.Count -gt 0) {
            return $this.Children[0]
        }
        return $null;
    }

    [BaseItem] LastChild() {
        if($this.Children.Count -gt 0) {
            return $this.Children[$this.Children.Count - 1]
        }
        return $null;
    }

    [BaseItem] GetNext() {
        return $this.Next
    }

    [BaseItem] GetPrev() {
        return $this.Prev
    }

    [void] Add([BaseItem] $Item) {
        $last = $this.LastChild()
        if($null -ne $last) {$last.Next = $Item;}
        $Item.Prev = $last
        $Item.Parent = $this;

        $this.Children += $Item;
    }

    [BaseItem] GetParent() {
        return $this.Parent;
    }

    [string] ToString() {
        return "$($this.GetType().Name)`r`n"
    }

    [string] ToCsv() {
        return '';
    }

    [string] ToMarkdown([bool] $GithubFlawor) {
        return '';
    }

    [guid] GetId() {
        return $this.Id
    }

    [hashtable[]] GetProperties() {
        return $this.Properties
    }

    static [object] GetFromChainOfNames([object]$obj, [string]$types) {
        $cur = $types.Split('.')
        foreach($i in $obj) {
            if($i.GetType().Name -eq $cur[0]) {
                $tmp = ''
                if($cur.Count -gt 1) {
                    for($j=1; $j -lt $cur.Count; $j++) {
                        $tmp += "$($cur[$j])."
                    }
                    $ret = [BaseItem]::GetFromChainOfNames($i,$tmp.TrimEnd('.'))
                    if($null -ne $ret) {return $ret;}
                }
                return $i;
            }
        }
        return $null;
    }

    [BaseItem] FindById([guid] $Id) {
        if($null -ne $this.Children) {
            foreach($p in $this.Children) {
                $ret = $p.FindById($Id)
                if($null -ne $ret) { return $ret}
            }
            return $null
        }
        return $null
    }    

    [BaseItem] Find([BaseItem] $Item) {
        if($this.Children.Count -gt 0) {
            foreach($c in $this.Children) {
                if($c.GetId() -eq $Item.GetId()) {return $c;}                
            }
            return $null;
        }
        return $null;
    }

    [string] Get11tyFileName () {
        return '';
    }

    <#
    static [bool] ParagraphIsProcessed($para) {
        $processed = $false
        $run = $para | ?{$_.GetType().Name -eq 'Run'}
        if($null -ne $run) {
            $tmp = [ParagraphItem]::GetItemType($run)
            if($null -ne $tmp -and $tmp -eq 'Text' -or $tmp -eq 'Drawing') {
                $tmp = $run.Annotation('psobject')
                $tmp1 = $para.Annotation('psobject')
                if(($null -ne $tmp -and $tmp.Processed -eq $true) -or ($null -ne $tmp1 -and $tmp1.Processed -eq $true)) { 
                    $processed = $true
                }
            }
        }
        return $processed
    }

    static [void] ParagraphMarkAsProcessed($para) {
        $run = $para | ?{$_.GetType().Name -eq 'Run'}
        $tmp = [ParagraphItem]::GetItemType($run)
        if($null -ne $tmp -and $tmp -eq 'Text' -or $tmp -eq 'Drawing') {
            if($null -ne $run) {
                $tmp = $run.AddAnnotation((New-Object PSObject -Property @{Really = $true}))
                $tmp = $para.AddAnnotation((New-Object PSObject -Property @{Really = $true}))
            }
        }
    }
    #>
}

class DocumentItem : BaseItem {
    [string] $Path
    [string] $MdDir
    $Cfg
    $AllPlugins
    $ImgStream
    [System.IO.FileStream] $Stream
    [object] $Document
    [string[]] $XlsTabNames
    [string] $DocType # Workbook, Presentation or Document
    $datToChange = @(14, 22, 30, 165, 167, 168, 174, 175, 182, 183, 186)
    $numToChange = @(164)
    $NumberFormatId = @{
        '164' = '#,###,###,###,###.00'; # currency. My own format
        '165' = '#,###,###,###,###.00'; # currency. My own format
        '166' = '#,###,###,###,###.00'; # currency. My own format
    }
    $DateFormatId = @{
        '14' = "dd/MM/yyyy";
        '15' = "d-MMM-yy";
        '16' = "d-MMM";
        '17' = "MMM-yy";
        '18' = "h:mm AM/PM";
        '19' = "h:mm:ss AM/PM";
        '20' = "h:mm";
        '21' = "h:mm:ss";
        '22' = "M/d/yy h:mm";
        '30' = "M/d/yy";
        '34' = "yyyy-MM-dd";
        '45' = "mm:ss";
        '46' = "[h]:mm:ss";
        '47' = "mmss.0";
        '51' = "MM-dd";
        '52' = "yyyy-MM-dd";
        '53' = "yyyy-MM-dd";
        '55' = "yyyy-MM-dd";
        '56' = "yyyy-MM-dd";
        '58' = "MM-dd";        
        #'165' = "M/d/yy";
        #'166' = "dd MMMM yyyy";
        '167' = "dd/MM/yyyy";
        '168' = "dd/MM/yy";
        '169' = "d.M.yy";
        '170' = "yyyy-MM-dd";
        '171' = "dd MMMM yyyy";
        '172' = "d MMMM yyyy";
        '173' = "M/d";
        '174' = "M/d/yy";
        '175' = "MM/dd/yy";
        '176' = "d-MMM";
        '177' = "d-MMM-yy";
        '178' = "dd-MMM-yy";
        '179' = "MMM-yy";
        '180' = "MMMM-yy";
        '181' = "MMMM d, yyyy";
        '182' = "M/d/yy hh:mm t";
        '183' = "M/d/y HH:mm";
        '184' = "MMM";
        '185' = "MMM-dd";
        '186' = "M/d/yyyy";
        '187' = "d-MMM-yyyy";
    }
    
    DocumentItem([string]$Path, [string]$MdDir, [string]$CfgFile, [string[]]$AllPlugins) : base($null, $null) {
        $this.Path = $Path;
        $this.MdDir = $MdDir;
        $this.XlsTabNames = @()
        $this.AllPlugins = $AllPlugins

        Write-Host 'OpenDoc: Author: Alexander Ivanov [Alexander.Ivan0v@outlook.com] https://gitlab.com/VeryComplexName/opendoc' -Fore green

        $culture = [System.Globalization.CultureInfo]::CurrentCulture        
        foreach($d in $this.datToChange) {
            if($this.DateFormatId.Keys -contains $d.ToString()) {
                $this.DateFormatId[$d.ToString()] = $this.DateFormatId[$d.ToString()].Replace('/', $culture.DateTimeFormat.DateSeparator)
            }
        }
        foreach($d in $this.numToChange) {
            if($this.NumberFormatId.Keys -contains $d.ToString()) {
                $this.NumberFormatId[$d.ToString()] = $this.NumberFormatId[$d.ToString()].Replace(',', $culture.NumberFormat.NumberGroupSeparator)#.Replace('.', $culture.NumberFormat.NumberDecimalSeparator)
            }
        }        

        if((Test-Path $CfgFile)) {$this.Cfg = Get-Content $CfgFile -Raw | ConvertFrom-Json}
        else {throw "There is no such configuration file [$CfgFile]"}
    }

    [void] CloseDocument() {
        if($null -ne $this.Stream) {$this.Stream.Dispose(); $this.Stream = $null;} 
        if($null -ne $this.Document) {$this.Document.Dispose(); $this.Document = $null;}
    }

    [string[]] GetPluginsFunctionName([string]$functionName) {
        $ret = @()

        if($null -ne $this.AllPlugins) {
            foreach($m in $this.AllPlugins) {
                $func = $functionName.Split('-')
                if($func.Count -eq 2) {
                    $mName = [System.IO.Path]::GetFileNameWithoutExtension($m)
                    $moduleDirName = Split-Path (Get-Module $mName).ModuleBase -Leaf
                    $functionName = "$($func[0])-_$($moduleDirName)_$($func[1])"
                    if($null -ne (Get-Command $functionName -Module $mName)) {
                        $ret += $functionName;
                    }
                }
            }
        }

        return $ret;
    }

    [object] CallPlugin([string]$functionName, [object[]]$parameters) {

        $funcNames = $this.GetPluginsFunctionName($functionName)
        foreach($f in $funcNames) {
            $tmp = $null;
            try {
                $parameters = . $f $parameters
            }
            catch {
                toLog "$($f): $($_ | Out-String)" error
            }
        }

        return $parameters
    }

    [bool] IsPluginPresent([string]$functionName) {
        $ret = $false

        if($null -ne $this.AllPlugins) {
            foreach($m in $this.AllPlugins) {
                $func = $functionName.Split('-')
                if($func.Count -eq 2) {
                    $mName = [System.IO.Path]::GetFileNameWithoutExtension($m)
                    $moduleDirName = Split-Path (Get-Module $mName).ModuleBase -Leaf
                    $functionName = "$($func[0])-_$($moduleDirName)_$($func[1])"
                    if($null -ne (Get-Command $functionName -Module $mName)) {
                        $ret = $true;
                        break;
                    }
                }
            }
        }
        
        return $ret
    }    

    [string] GetInternalStyleFor([string]$StyleName, [object]$obj) { # FASEHead1
        if($null -eq $obj) {$obj = $this.Cfg}

        foreach($p in $obj.PSObject.Properties) {
            if($p.Value.GetType().Name -eq 'Object[]') {
                if($StyleName -in $p.Value) {
                    return "$($p.Name)"
                }
            }
            elseif($p.Value.GetType().Name -eq 'PSCustomObject') {
                $ret = $this.GetInternalStyleFor($StyleName, $p.Value)
                if(![string]::IsNullOrEmpty($ret)) {return $ret}
            }
        }
        return $null;
    }

    [string] GetPath() {
        return $this.Path;
    }

    [string] ToString() {
        return "Document from: [$($this.Path)] MarkdownDirectory: [$($this.MdDir)]`r`n";
    }

    [string] NormilizeStr([string] $str) {
        # strip all tripling of "`r`n"
        $trimIt = "`r`n`r`n`r`n"
        $trimTo = "`r`n`r`n"

        $found = $true
        while($found) {
            $str = $str.Replace($trimIt, $trimTo)
            $found = ($str.IndexOf($trimIt) -ge 0)
        }
        return $str
    }

    [string] ToMarkdown([bool] $GithubFlawor) {
        $listCnt = [DocumentItem]::InitListCounters();
        $content = '';

        $sheetIdx = 0;
        foreach($c in $this.Children) {
            if($c.GetType().Name -eq 'ParagraphItem') {
                $content += $c.ToMarkdown($GithubFlawor, $listCnt)
                # Paragraph
                if($c.Properties.Style -eq 'List') {
                    # List
                    if($c.Properties.ListLevel -ge 0) {
                        $listCnt.($c.Properties.ListLevel)++;
                    }
                }
                else {
                    # NOT a List
                    $listCnt = [DocumentItem]::InitListCounters();
                }
            }
            else {
                # Table or ImgItem (for pptx)
                $listCnt = [DocumentItem]::InitListCounters();
                if($c.GetType().Name -ne 'ImgItem') {
                    # Table
                    if($this.DocType -eq 'Workbook') {
                        if($this.Cfg.Workbook.SheetHeading.UseTabName) {
                            $hdr = $this.Cfg.Workbook.SheetHeading.Template.Replace('{TabName}', $this.XlsTabNames[$sheetIdx])
                            $content += "`r`n`r`n$hdr`r`n`r`n"
                            $sheetIdx++;
                        }
                    }
                    $content += $c.ToMarkdown($GithubFlawor)
                }
                else {$content += "`r`n" + $c.ToMarkdown($GithubFlawor, $listCnt) + "`r`n"}
            }
        }
        return $this.NormilizeStr($content)
    }

    [void] SaveToCsv([string]$CsvDir) {
        $sheetIdx = 0;
        foreach($c in $this.Children) {
            if($c.GetType().Name -ne 'ParagraphItem' -and $c.GetType().Name -ne 'ImgItem') {
                # Table
                if($this.DocType -eq 'Workbook') {
                    if($this.Cfg.Workbook.CreateCsv) {
                        $FileName = Join-Path $CsvDir "$([System.IO.Path]::GetFileNameWithoutExtension($this.Path)).$($this.XlsTabNames[$sheetIdx]).csv"
                        $c.ToCsv() | Out-File $FileName
                        $sheetIdx++;
                    }
                }
            }
        }
    }
    
    [void] SaveToText([string] $FileName) {
        toLog "Saving [$($this.Path)] to [$FileName] as Text..."
        $content = $this.ToString()
        foreach($c in $this.Children) {
            $content += $c.ToString()
        }
        $content | Out-File $FileName
        toLog "Saved to [$FileName]"
    }

    static [hashtable] InitListCounters() {
        return  @{0=0; 1=0; 2=0; 3=0; 4=0; 5=0; 6=0; 7=0; 8=0;};
    }

    [void] SaveToMarkdown([string] $FileName, [bool] $GithubFlawor) {
        toLog "Saving [$($this.Path)] to [$FileName] as Markdown..."
        $this.ToMarkdown($GithubFlawor) | Out-File $FileName
        toLog "Saved to [$FileName]"
    }

    [string] Get11tyKey() {
        return [System.IO.Path]::GetFileNameWithoutExtension($this.Path);
    }

    [string] Get11tyFileName([string] $DirName) {
        return (Join-Path $DirName "$([System.IO.Path]::GetFileNameWithoutExtension($this.Path).Replace(' ', '_')).md")
    }

    [string] Make11tyHeader() {
        $ret = @"
---
layout: base.njk
title: "$([System.IO.Path]::GetFileNameWithoutExtension($this.Path))"
translationKey: "$([System.IO.Path]::GetFileNameWithoutExtension($this.Path))"
eleventyNavigation:
    key: "$([System.IO.Path]::GetFileNameWithoutExtension($this.Path))"
    order: "0"
---
        
"@
        return $ret
    }

    [hashtable] GetCorrectCnt([string] $Style, [hashtable] $Heading, [string] $Key) {
        $ret = @{Order = 0; ParentKey = ''}
        switch($Style) {
            'Heading1' {
                $heading.h1.Order++;
                $ret.Order = $heading.h1.Order;
                $ret.ParentKey = $heading.h0.Key
                $heading.h1.Key = $Key
                $heading.h2 = @{Order=0; Key=''}; ;
                $heading.h3 = @{Order=0; Key=''}; ;
                $heading.h4 = @{Order=0; Key=''}; ;
                $heading.h5 = @{Order=0; Key=''}; ;
                $heading.h6 = @{Order=0; Key=''}; ; 
                break;
            }
            'Heading2' {
                $heading.h2.Order++;
                $ret.Order = $heading.h2.Order;
                $ret.ParentKey = $heading.h1.Key
                $heading.h2.Key = $Key
                $heading.h3 = @{Order=0; Key=''}; ;
                $heading.h4 = @{Order=0; Key=''}; ;
                $heading.h5 = @{Order=0; Key=''}; ;
                $heading.h6 = @{Order=0; Key=''}; ; 
                break;
            }
            'Heading3' {
                $heading.h3.Order++;
                $ret.Order = $heading.h3.Order;
                $ret.ParentKey = $heading.h2.Key
                $heading.h3.Key = $Key
                $heading.h4 = @{Order=0; Key=''}; ;
                $heading.h5 = @{Order=0; Key=''}; ;
                $heading.h6 = @{Order=0; Key=''}; ; 
                break;
            }
            'Heading4' {
                $heading.h4.Order++;
                $ret.Order = $heading.h4.Order;
                $ret.ParentKey = $heading.h3.Key
                $heading.h4.Key = $Key
                $heading.h5 = @{Order=0; Key=''}; ;
                $heading.h6 = @{Order=0; Key=''}; ; 
                break;
            }
            'Heading5' {
                $heading.h5.Order++;
                $ret.Order = $heading.h5.Order;
                $ret.ParentKey = $heading.h4.Key
                $heading.h5.Key = $Key
                $heading.h5 = @{Order=0; Key=''}; ;
                $heading.h6 = @{Order=0; Key=''}; ; 
                break;
            }
            'Heading6' {
                $heading.h6.Order++;
                $ret.Order = $heading.h6.Order;
                $ret.ParentKey = $heading.h5.Key
                $heading.h6.Key = $Key
                break;
            }
        }
        return $ret;
    }

    [void] SaveToMarkdownFor11ty([string] $DirName, [bool] $GithubFlawor) {
        toLog "Saving [$($this.Path)] to [$DirName] as Markdown with division by Headings..."
        $tempMdDir = $this.MdDir
        $this.MdDir = $DirName

        if((Test-Path $DirName)) {del $DirName -Force -Recurse -Confirm:$false}
        md $DirName -Force -Confirm:$false

        $currentFileName = $this.Get11tyFileName($DirName);
        $content = $this.Make11tyHeader()

        $content += $this.ToMarkdown($GithubFlawor);        
        $listCnt = [DocumentItem]::InitListCounters();
        $heading = @{
            h0=@{Order=0; Key=$this.Get11tyKey()}; 
            h1=@{Order=0; Key=''}; 
            h2=@{Order=0; Key=''}; 
            h3=@{Order=0; Key=''}; 
            h4=@{Order=0; Key=''}; 
            h5=@{Order=0; Key=''}; 
            h6=@{Order=0; Key=''};
        }

        foreach($c in $this.Children) {
            if($c.GetType().Name -eq 'ParagraphItem') {
                
                # Collecting before first Heading paragraph
                if($c.Properties.Style.StartsWith('Heading')) {
                    $ret = $this.GetCorrectCnt($c.Properties.Style, $heading, $c.Get11tyKey())
                    # Flush buffer
                    $content | Out-File $currentFileName
                    $content = ''
                    # Start to write to the New file
                    $currentFileName = $c.Get11tyFileName($DirName);
                    $content = $c.Make11tyHeader($ret)
                }

                $content += $c.ToMarkdown($GithubFlawor, $listCnt)
                # Paragraph
                if($c.Properties.Style -eq 'List') {
                    # List
                    if($c.Properties.ListLevel -ge 0) {
                        $listCnt.($c.Properties.ListLevel)++;
                    }
                }
                else {
                    # NOT a List
                    $listCnt = [DocumentItem]::InitListCounters();
                }
            }
            else {
                #Table
                $listCnt = [DocumentItem]::InitListCounters();
                $content += $c.ToMarkdown($GithubFlawor)
            }
        }
        $content | Out-File $currentFileName
        $this.MdDir = $tempMdDir
        toLog "Saved to [$DirName]"
    }

    [void] SaveTo([string] $Format, [string] $FileOrDirName, [bool] $GithubFlawor) {
        switch($Format) {
            'Text' {
                $this.SaveToText($FileOrDirName)
                break;
            }
            'Markdown' {
                $this.SaveToMarkdown($FileOrDirName, $GithubFlawor)
                break;
            }
            '11ty' {
                $this.SaveToMarkdownFor11ty($FileOrDirName, $GithubFlawor)
                break;
            }
        }
    }

    [void] Parse($item) {
        switch($item.GetType().Name) {
            'Paragraph' {                
                #if(!([BaseItem]::ParagraphIsProcessed($item))) {
                    toLog "Document.Parse: Paragraph found"
                    $this.Add([ParagraphItem]::new($this, $this).Parse($item))
                #    [BaseItem]::ParagraphMarkAsProcessed($item)
                #}
                break;
            }
            'Table' {
                toLog "Document.Parse: Table found"
                $this.Add([TableItem]::new($this, $this).Parse($item))
                break;
            }
        }
    }

    [void] ParsePptx($item) {
        if($item.RelationshipId.HasValue) {
            $val = $item.RelationshipId.Value
            $part = $this.Document.PresentationPart.Parts | ? {$_.RelationshipId -eq $item.RelationshipId.Value}
            if($null -ne $part) {
                $slidePart = $this.Document.PresentationPart.GetPartById($part.RelationshipId)
                if($null -ne $slidePart) {
                    foreach($element in $slidePart.Slide.CommonSlideData.ShapeTree | ? {$_.GetType().Name -eq 'Shape' -or $_.GetType().Name -eq 'GraphicFrame' -or $_.GetType().Name -eq 'Picture'}) {                        
                     $txt = $element | ? {$_.GetType().Name -eq 'TextBody'}
                        if($null -ne $txt -and $null -ne ($txt | ?{$_.GetType().Name -eq 'Paragraph'})) {
                            $this.Parse(($txt | ?{$_.GetType().Name -eq 'Paragraph'}))
                        }                            
                        else {
                            $graph = $element | ? {$_.GetType().Name -eq 'Graphic'}
                            if($null -ne $graph) {
                                $tbls = $graph.GraphicData | ? {$_.GetType().Name -eq 'Table'}
                                if($null -ne $tbls) {
                                    $this.Parse($tbls)
                                }
                            }
                            elseif($element.GetType().Name -eq 'Picture') {
                                toLog "Document.ParsePptx: Picture found"                                
                                $this.Add([ImgItem]::new($this, $this).Parse($element));
                            }
                        }
                        
                    }
                    #foreach ($element in $slidePart.Slide.Descendants() | ?{$_.GetType().Name -eq 'Paragraph' -or $_.GetType().Name -eq 'Table'<# -or $_.GetType().Name -eq 'TextBody'#>}) {                        
                    #    $this.Parse($element)
                    #}
                }
            }
        }
        else {toLog "RelationshipId has no value" 'warning'}
    }

    static [hashtable] SplitCellIdToXY([string] $CellId) {
        $ret = @{col=0; row=0;}

        $Col = ''; $Row = '';
        for ($i = 0; $i -lt $CellId.Length; $i++) { # A2, BS34, ...
            if([int]($CellId[$i]) -ge 65 -and [int]($CellId[$i]) -le 90) { # A - Z
                $Col += $CellId[$i]
            }
            else {
                if([int]($CellId[$i]) -ge 48 -and [int]($CellId[$i]) -le 57) { # 0 - 9
                    $Row += $CellId[$i]
                }
            }
        }

        $ret.Col = [DocumentItem]::ColumnNameToNumber($Col)
        $ret.Row = [int]($Row - 1)

        return $ret;
    }
    
    # Return the column number for this column name.
    static [int] ColumnNameToNumber([string] $col_name)
    {
        $basis = 26

        [int] $result = 0;

        # Process each letter.
        for ($i = 0; $i -lt $col_name.Length; $i++) {
            [char] $letter = $col_name[$i];

            # See if it's out of bounds.
            if ($letter -lt 'A') {$letter = 'A';}
            if ($letter -gt 'Z') {$letter = 'Z';}

            # Add in the value of this letter.
            if($i -lt ($col_name.Length - 1)) {$result += ([int]($letter) - 65 + 1) * $basis}
            else {$result += [int]($letter) - 65;}
        }
        return $result;
    }

    [Paragraph] GetCellValue($wbPart, $cell) {
        if($null -ne $cell.DataType) {
            switch($cell.DataType.Value) {
                'SharedString' {
                    [object[]] $allSharedStrings = $wbPart.SharedStringTablePart.SharedStringTable.ChildElements;
                    if($null -ne ($allSharedStrings[$cell.CellValue.Text] | ? {$_.GetType().Name -eq 'Run'})) {
                        return [Paragraph]::new().SetRuns(($allSharedStrings[$cell.CellValue.Text] | ? {$_.GetType().Name -eq 'Run'}))
                    }
                    return [Paragraph]::new().SetText($allSharedStrings[$cell.CellValue.Text].InnerText)
                    break;
                }
                'Boolean' {                
                    if($cell.CellValue.Text -eq '0') {return [Paragraph]::new().SetText('FALSE')}
                    else {return [Paragraph]::new().SetText('TRUE')}
                    break;
                }
                'Date' {
                    break;
                }
                'Error' {
                    break;
                }
                'InlineString' {
                    break;
                }
                'Number' {
                    break;
                }
                'String' {
                    break;
                }
            }
        }
        elseif($null -ne $cell.StyleIndex -and $cell.StyleIndex.HasValue) {
            # Date ?
            $formatId = $wbPart.WorkbookStylesPart.Stylesheet.CellFormats.ChildElements[$cell.StyleIndex.Value].NumberFormatId
            if($this.DateFormatId.Keys -contains $formatId.Value) {
                return [Paragraph]::new().SetText([DateTime]::FromOADate($cell.CellValue.Text).ToString($this.DateFormatId[$formatId.InnerText]))
            }
            elseif($this.NumberFormatId.Keys -contains $formatId.Value) {
                $val = 0
                try {
                    $culture = [System.Globalization.CultureInfo]::CurrentCulture
                    $val = [double]::Parse($cell.CellValue.Text.Replace('.', $culture.NumberFormat.NumberDecimalSeparator))
                } catch {}
                return ([Paragraph]::new().SetText("{0:$($this.NumberFormatId[$formatId.InnerText])}" -f $val))
            }
        }
        return [Paragraph]::new().SetText($cell.CellValue.Text)
    }

    [Table] FillAllCellsInTable($wbPart, $table, [hashtable] $leftTop, [hashtable] $rightBottom) {
        $ret = [Table]::new();
        $rows = $table | ?{$_.GetType().Name -eq 'Row'}
        for($row = $leftTop.Row; $row -le $rightBottom.Row; $row++) {
            $curRow = [Row]::new(); $ret.Add($curRow)
            for($col = $leftTop.Col; $col -le $rightBottom.Col; $col++) {
                $curCell = [Cell]::New(); $curRow.Add($curCell)
                $r = $rows | ? {$_.RowIndex.Value -eq ($row + 1)}
                if($null -ne $r) {
                #if($null -ne $rows[$row - $leftTop.Row]) {                
                    #$r = $rows[$row - $leftTop.Row]
                    $cells = $r | ? {$_.GetType().Name -eq 'Cell'}
                    if($null -ne $cells) {
                        $found = $false
                        if($cells.GetType().Name -eq 'Cell') {
                            $tmp = [DocumentItem]::SplitCellIdToXY($cells.CellReference.Value)
                            if($tmp.Row -eq $row -and $tmp.Col -eq $col) {
                                $curCell.SetPara($this.GetCellValue($wbPart, $cells))
                                $found = $true;
                            }
                        }
                        elseif($cells.GetType().Name -eq 'Object[]') {
                            foreach($cell in $cells) {
                                $tmp = [DocumentItem]::SplitCellIdToXY($cell.CellReference.Value)
                                if($tmp.Row -eq $row -and $tmp.Col -eq $col) {
                                    $curCell.SetPara($this.GetCellValue($wbPart, $cell))
                                    $found = $true; 
                                    break;
                                }
                            }
                        }
                    }
                    else {
                        # Add all required fake cells
                        $curRow.Remove($curCell); $curCell = $null;
                        for($col = $leftTop.Col; $col -le $rightBottom.Col; $col++) {$curRow.Add([Cell]::new())}
                    }
                }
                else {
                    ## Add all required fake cells
                    $curRow.Remove($curCell); $curCell = $null;
                    for($col = $leftTop.Col; $col -le $rightBottom.Col; $col++) {$curRow.Add([Cell]::new())}
                }
            }
        }

        return $ret;
    }

    [void] ParseXls($item) {
        if($item.Id.HasValue) {
            $wbPart = $this.Document.GetPartById($item.Id.Value);
            if($null -ne $wbPart) {
                $sheetIdx = $wbPart.Workbook.Sheets.Name.Count - 1;
                foreach($wSheetPart in $wbPart.WorksheetParts) {
                    $this.XlsTabNames += $wbPart.Workbook.Sheets.Name[$sheetIdx].Value
                    toLog "Found tab: $($wbPart.Workbook.Sheets.Name[$sheetIdx].Value)"
                    $wSheet = $wSheetPart.Worksheet
                    # SheetDimension, SheetData
                    $table = [BaseItem]::GetFromChainOfNames($wSheetPart.Worksheet, 'SheetData')
                    $sheetDim = [BaseItem]::GetFromChainOfNames($wSheetPart.Worksheet, 'SheetDimension')
                    if($null -ne $table -and $null -ne $sheetDim) {
                        # Calc left top & right bottom corners
                        $tmp = $sheetDim.Reference.Value.ToString().Split(':')
                        $leftTop = [DocumentItem]::SplitCellIdToXY($tmp[0]);
                        $rightBottom = [DocumentItem]::SplitCellIdToXY($tmp[1]);
                        toLog "Data dimension: [$($rightBottom.Col - $leftTop.Col + 1)x$($rightBottom.Row - $leftTop.Row + 1)]"
                        # Fill if Cells not enough
                        $table = $this.FillAllCellsInTable($wbPart, $table, $leftTop, $rightBottom)
                        $this.Add([TableItem]::new($this, $this).Parse($table));
                    }
                    $sheetIdx--;
                }
            }
        }
    }
    
    [void] ProcessDocument() {
        $tmp = $this.OpenDocument($false);
        $this.Stream = $tmp.Stream;
        $this.Document = $tmp.Document;
        if($null -eq $this.Document) {toLog "Unable to read document: [$($this.Path)]" 'error'}
        else {   
            $this.DocType = $this.Document.DocumentType.ToString()
            switch($this.Document.DocumentType) {
                'Document' {
                    $doc = $this.Document.MainDocumentPart.Document
                    foreach($item in $doc.Body.ChildElements) {$this.Parse($item) } 
                    break;
                }
                'Presentation' { 
                    $doc = $this.Document.PresentationPart.Presentation
                    foreach($item in [BaseItem]::GetFromChainOfNames($doc.ChildElements, 'SlideIdList')) {$this.ParsePptx($item) }
                    break;
                }
                'Workbook' { 
                    $doc = $this.Document.WorkbookPart.Workbook
                    foreach($item in [BaseItem]::GetFromChainOfNames($doc.ChildElements, 'Sheets')) {$this.ParseXls($item) }
                    break;
                }
            }
        }
    }

    [hashtable] OpenDocument([bool]$isEditable = $false) {
        toLog "Opening document: [$($this.Path)]..."

        if(!(Test-Path $this.Path)) {throw "There is no such file: [$($this.Path)]"}
    
        [System.IO.FileStream]$fileStream = [System.IO.File]::Open($this.Path, [System.IO.FileMode]::Open, [System.IO.FileAccess]::Read, [System.IO.FileShare]::Read)
        if($null -eq $fileStream) {throw "Can't read file [$($this.Path)]"}
    
        switch -Regex ([System.IO.Path]::GetExtension($this.Path)) {
            'docx|dotm|dotx' {
                [DocumentFormat.OpenXml.Packaging.WordprocessingDocument] $doc = $null
                #String path to doc, bool isEditable
                $doc = [DocumentFormat.OpenXml.Packaging.WordprocessingDocument]::Open($fileStream, $isEditable)
                break;
            }
    
            'xlsx|xlsm|xltx|xltm' {
                [DocumentFormat.OpenXml.Packaging.SpreadsheetDocument] $doc = $null
                $doc = [DocumentFormat.OpenXml.Packaging.SpreadsheetDocument]::Open($fileStream, $isEditable)
                break;
            }
    
            'pptx|pptm|potx|potm|ppsx' {
                [DocumentFormat.OpenXml.Packaging.PresentationDocument] $doc = $null
                $doc = [DocumentFormat.OpenXml.Packaging.PresentationDocument]::Open($fileStream, $isEditable)
                break;
            }
        }
        toLog "Document: [$($this.Path)] opened"
        return @{Stream = $fileStream; Document = $doc}
    }
}

class ParagraphItem : BaseItem {
    [object[]] $Children
    [bool] $OneTimeOp
    
    ParagraphItem([DocumentItem] $Document, [BaseItem] $Parent) : base($Document, $Parent) {
        $this.Properties = @{
            TextAlignment = '';
            Style = '';
            Justification = '';
        }
        $this.OneTimeOp = $false
    }

    [string] GetPureString() {
        $c = $this.FirstChild();
        $ret = ''
        while($null -ne $c) {                    
            if(($c | gm  | Select TypeName -First 1).TypeName -eq 'StringItem') {$ret += $c.ToString()}
            $c = $c.GetNext()
        }
        return $ret
    }

    [string] Get11tyKey() {
        return $this.GetPureString();
    }

    [string] Get11tyFileName([string] $DirName) {
        if($this.Properties.Style.StartsWith('Heading')) {
            $ret = $this.GetPureString()
            return (Join-Path $DirName "$($ret.Replace('/', '_').Replace('\', '_').Replace(':', '_').Replace(' ', '_').Replace('?', '_').Replace('*', '_')).md")
        }
        return $null;
    }

    [string] Make11tyHeader([hashtable] $Data) {
        $ret = ''
        if($this.Properties.Style.StartsWith('Heading')) {

            $str = $this.Get11tyKey()            
            
            $ret = @"
---
layout: base.njk
title: "$str"
translationKey: "$str"
eleventyNavigation:
    key: "$str"
    parent: "$($Data.ParentKey)"    
    order: "$($Data.Order)"
---
        
"@
        }
        return $ret
    }

    [void] SetTextAlignment($TextAlignment) {
        $this.Properties.TextAlignment = $TextAlignment
    }

    [string] ToString() {
        $ret = "`r`n`r`n" 
        foreach($c in $this.Children) {$ret += $c.ToString()}
        $ret += "`r`n`r`n"
        return $ret;
    }

    [string] ToMarkdown([bool] $GithubFlawor, [hashtable] $listCounter) {
        $this.OneTimeOp = $false
        $ret = "`r`n" 

        foreach($c in $this.Children) { 
            $ret += $c.ToMarkdown($GithubFlawor, $listCounter)
        }
        if($this.ToString().IndexOf('****') -lt 0) {$ret = $ret.Replace('****', '')}

        # Especial for Lists
        if($this.Properties.Style -eq 'List') {
            if(!('ListLevel' -in $this.Properties.GetEnumerator().Name)) {$this.Properties.ListLevel = 0;}
            $nxt = $this.GetNext()
            if($null -ne $nxt -and $nxt.Properties.Style -ne 'List') {$ret += "`r`n"}
        }
        else {$ret += "`r`n"}

        $this.OneTimeOp = $false
        
        return $ret;
    }
    
    [ParagraphItem] Parse($item) {
        foreach($i in $item) {
            switch($i.GetType().Name) {
                'ParagraphProperties' {
                    $this.Properties.TextAlignment = $i.TextAlignment
                    if($null -ne $i.Justification -and $i.Justification.Val.HasValue) {
                        $this.Properties.Justification = $i.Justification.Val.Value
                    }
                    $ParagraphStyleId = [BaseItem]::GetFromChainOfNames($i, 'ParagraphStyleId')
                    if($null -ne $ParagraphStyleId -and $ParagraphStyleId.val.HasValue) {
                        $internalStyle = $this.Document.GetInternalStyleFor($ParagraphStyleId.val.Value, $null)
                        if([string]::IsNullOrEmpty($internalStyle)) {$internalStyle = 'Normal'}
                        $this.Properties.Style = $internalStyle
                    }
                    $NumberingLevelReference = [BaseItem]::GetFromChainOfNames($i, 'NumberingProperties.NumberingLevelReference')
                    if($null -ne $NumberingLevelReference -and $NumberingLevelReference.val.HasValue) {
                        $this.Properties.ListLevel = $NumberingLevelReference.val.Value
                    }
                    $NumberingId = [BaseItem]::GetFromChainOfNames($i, 'NumberingProperties.NumberingId')
                    if($null -ne $NumberingId -and $NumberingId.val.HasValue) {
                        $this.Properties.NumberingId = $NumberingId.val.Value
                        # Numbering (List, Heading)
                        $numId = $NumberingId.val.Value
                        $num = $this.Document.Document.MainDocumentPart.NumberingDefinitionsPart.Numbering 
                        $absNumId = $num | ?{$_.GetType().Name -eq 'NumberingInstance' -and $_.NumberId -eq $numId}
                        $absNum = $num | ?{$_.GetType().Name -eq 'AbstractNum' -and $_.AbstractNumberId -eq $absNumId.Val.Value}
                        $this.Properties.ListNumbering = $absNum # $absNum | ? {$_.GetType().Name -eq 'Level'} - all about each level only
                        # https://stackoverflow.com/questions/20728408/how-do-you-access-the-numbering-of-an-outline-in-a-word-document-using-c-sharp-a
                    }
                    break;
                }
                'Run' {
                    switch([ParagraphItem]::GetItemType($i)) {
                        'Text' {
                            $this.Add([StringItem]::new($this.Document, $this).Parse($i))
                            break;
                        }
                        'Drawing' {
                            $this.Add([ImgItem]::new($this.Document, $this).Parse($i))
                            break;
                        }
                    }
                    break;
                }
                'Hyperlink' {
                    if($null -ne $i.Id -and $i.Id.HasValue) {
                        $hyper = $this.Document.Document.MainDocumentPart.GetReferenceRelationship($i.Id.Value)
                        $tmp = $hyper.Uri.ToString()
                        if($this.Document.Cfg.Url.EscapeChars) {$tmp = [uri]::EscapeUriString($tmp)}
                        $this.Add([StringItem]::new($this.Document, $this).Parse($i, $tmp))
                    }
                    else {$this.Add([StringItem]::new($this.Document, $this).Parse($i, '<error reading>'))}
                }
                'SdtRun' {
                    switch([ParagraphItem]::GetItemType($i)) {
                        'SdtProperties' {
                            if($null -ne $i.SdtContentRun) {
                                foreach($tmp in $i.SdtContentRun) {
                                    if($tmp.GetType().Name -eq 'Run') {
                                        $this.Add([StringItem]::new($this.Document, $this).Parse($tmp))
                                    }
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }
        return $this;
    }

    static [string] GetItemType($item) {
        $ret = '';
        foreach($i in $item) {
            switch($i.GetType().Name) {
                'RunProperties' {
                    break;
                }
                'Text' {
                    $ret = 'Text'
                    break;
                }
                'Drawing' {
                    $ret = 'Drawing'
                    break;
                }
                'SdtProperties' {
                    $ret = 'SdtProperties'
                    break;
                }
                'SdtContentRun' {
                    $ret = 'SdtContentRun'
                    break;
                }
            }
            if(![string]::IsNullOrEmpty($ret)) {return $ret;}
        }
        return $ret;
    }
}

class CellItem : BaseItem{
    [ParagraphItem[]] $Content

    CellItem([DocumentItem] $Document, [BaseItem] $Parent) : base($Document, $Parent) {
    }

    [string] ToString() {
        return ''
        #return ($this.Content | %{"$($_.ToString()) "})
    }

    [string] ToCsv() {
        $ret = ''

        foreach( $c in $this.Content) {
            $ret += $c.ToString()
        }
        while($ret.EndsWith("`r") -or $ret.EndsWith("`n")) {
            $ret = $ret.Trim("`r").Trim("`n")
        }
        $ret = $ret.Replace("`r`n", '<br>')

        return $ret + ','
    }

    [string] ToMarkdown([bool] $GithubFlawor) {
        $ret = ''
        $listCnt = [DocumentItem]::InitListCounters();
        foreach( $c in $this.Content) {
            $ret += $c.ToMarkdown($GithubFlawor, $listCnt)
            if($c.Properties.Style -eq 'List') {
                $listCnt.($c.Properties.ListLevel)++;
            }
            else {
                $listCnt = [DocumentItem]::InitListCounters();
            }
        }
        while($ret.EndsWith("`r") -or $ret.EndsWith("`n")) {
            $ret = $ret.Trim("`r").Trim("`n")
        }
        $ret = $ret.Replace("`r`n", '<br>')
        return $ret;
    }

    [CellItem] Parse([object[]]$item) {
        [object[]]$para = @($item | ?{$_.GetType().Name -eq 'Paragraph'})
        if($para.Count -eq 0) {
            $txtBody = $item | ?{$_.GetType().Name -eq 'TextBody'}
            if($null -ne $txtBody) {$para = @($txtBody | ?{$_.GetType().Name -eq 'Paragraph'})}
        }

        foreach($p in $para) {
            #if(!([BaseItem]::ParagraphIsProcessed($p))) {
                $this.Content += [ParagraphItem]::new($this.Document, $this).Parse($p)
            #    [BaseItem]::ParagraphMarkAsProcessed($p)
            #}
            
        }
        return $this
    }
}

class RowItem : BaseItem{
    [CellItem[]] $Cells

    RowItem([DocumentItem] $Document, [BaseItem] $Parent) : base($Document, $Parent){
    }

    [string] ToString() {
        $ret = "Cells: $($this.Cells.Count)`r`n"
        for($i=0; $i -lt $this.Cells.Count; $i++) {
            $ret += "Cell[$i]: $($this.Cells[$i].ToString())`r`n"
        }
        return $ret
    }

    [string] ToCsv() {
        $ret = ''

        for($i=0; $i -lt $this.Cells.Count; $i++) {
            $ret += $this.Cells[$i].ToCsv()
        }

        return $ret #.trimEnd(',')
    }

    [string] ToMarkdown([bool] $GithubFlawor, [hashtable] $Width) {
        $ret = ''
        
        for($i=0; $i -lt $this.Cells.Count; $i++) {
            $tmp = $($this.Cells[$i].ToMarkdown($GithubFlawor))
            if($tmp.Length -lt $Width.$i.Width) {
                $len = $tmp.Length
                for($j=0; $j -lt ($Width.$i.Width - $len); $j++) {$tmp += ' '}
            }
            $ret += "| $tmp "
        }
        $ret += "|`r`n"

        return $ret;
    }

    [RowItem] Parse([object[]]$item) {
        $cls = $null
        try {
            $cls = $item | ?{$_.GetType().Name -eq 'TableCell' -or $_.GetType().Name -eq 'Cell'}
        }
        catch{}

        if($null -ne $cls) {
            switch($cls.GetType().Name) {
                'Object[]' {
                    foreach($c in $cls) {
                        $this.Cells += [CellItem]::new($this.Document, $this).Parse($c)           
                    }
                    break;
                }
                {$_ -eq 'TableCell' -or $_ -eq 'Cell'} {
                    $this.Cells += [CellItem]::new($this.Document, $this).Parse($cls)
                    break;
                }
            }
        }

        return $this
    }
}

class TableItem : BaseItem{
    [RowItem[]] $Rows
    
    TableItem([DocumentItem] $Document, [BaseItem] $Parent) : base($Document, $Parent) {
    }

    [string] ToString() {
        $ret = "Table.Rows [$($this.Rows.Count)]`r`n"
        for($i=0; $i -lt $this.Rows.Count; $i++) {
            $ret += "Row[$i].$($this.Rows[$i].ToString())`r`n"
        }

        return "`r`n<$ret>`r`n"
    }

    [hashtable] CalcCellWidth([bool] $GithubFlawor) {
        $ret = @{}
        for($i=0; $i -lt $this.Rows[0].Cells.Count; $i++) {
            $align = ''
            if($this.Rows[0].Cells[$i].Content.Count -gt 0 -and $null -ne $this.Rows[0].Cells[$i].Content[0].Properties) {
                $align = $this.Rows[0].Cells[$i].Content[0].Properties.Justification
            }
            $ret.$i = @{
                Width = $this.Rows[0].Cells[$i].ToMarkdown($GithubFlawor).Length; 
                Align = $align
            }
            if($ret.$i.Width -lt 4) {$ret.$i.Width = 4}
        }

        if($this.Rows.Count -gt 1) {
            for($i=1; $i -lt $this.Rows.Count; $i++) {
                for($j=0; $j -lt $this.Rows[$i].Cells.Count; $j++) {
                    $newLen = $this.Rows[$i].Cells[$j].ToMarkdown($GithubFlawor).Length
                    if($newLen -gt $ret.$j.Width) {
                        $ret.$j.Width = $newLen
                    }
                }
            }
        }

        return $ret
    }

    [void] StandardizeTable() {
        toLog 'Trying to Standardize the current table...'
        $cellsCnt = $this.Rows[0].Cells.Count
        # find max
        $this.Rows | %{if($_.Cells.Count -gt $cellsCnt) {$cellsCnt = $_.Cells.Count}}
        # correct
        foreach($r in $this.Rows) {
            if($r.Cells.Count -lt $cellsCnt) {
                $iMax = $cellsCnt - $r.Cells.Count
                for($i=0; $i -lt $iMax; $i++) {
                    $r.Cells += [CellItem]::New($this.Document, $this)
                }
            }
        }
    }

    [void] StandardizeMergedCells() {
        # Test for merged cells
        $cellsCnt = $this.Rows[0].Cells.Count
        $this.Rows | %{
            if($_.Cells.Count -ne $cellsCnt) {
                if(!$this.Document.Cfg.Table.Standardize) {throw ""}
                else {$this.StandardizeTable();}
            }
        }
    }

    [string] ToMarkdown([bool] $GithubFlawor) {
        $ret = '';
        if($this.Rows.Count -gt 0) {
            try {
                $this.StandardizeMergedCells()
                $width = $this.CalcCellWidth($GithubFlawor)
                
                for($i=0; $i -lt $this.Rows.Count; $i++) {
                    $ret += $this.Rows[$i].ToMarkdown($GithubFlawor, $width)
                    # Header
                    if($i -eq 0) {                        
                        for($j=0; $j -lt $this.Rows[$i].Cells.Count; $j++) {
                            $ret += '| '
                            for($k=0; $k -lt $Width.$j.Width; $k++) {
                                if($k -eq 0 -and ![string]::IsNullOrEmpty($Width.$j.Align)) {
                                    if($Width.$j.Align -eq 'Left' -or $Width.$j.Align -eq 'Center') {
                                        $ret += ':'
                                    }
                                    else {$ret += '-'}
                                }
                                elseif($k -eq ($Width.$j.Width - 1) -and ![string]::IsNullOrEmpty($Width.$j.Align)) {
                                    if($Width.$j.Align -eq 'Right' -or $Width.$j.Align -eq 'Center') {
                                        $ret += ':'
                                    }
                                    else {$ret += '-'}
                                }
                                else {$ret += '-'}
                            }
                            $ret += ' '
                        }
                        $ret += "|`r`n"
                    }
                }
                # Excluding
                $tmp = $ret.Split("`r`n", [System.StringSplitOptions]::RemoveEmptyEntries)
                $ret = $tmp -Join "`r`n"
            }
            catch{
                toLog "Found table with merged cells. Will be marked in resultant document" 'warning'
                $ret = "`r`n-----`r`n`r`n<span style=`"color:red`">**Table has a different set of cells in one/more rows. It can't be shown in Markdown**</span>`r`n`r`n-----`r`n"
            }
        }
        return "`r`n$ret`r`n";
    }

    [string] ToCsv() {
        $ret = ''

        $this.StandardizeMergedCells()
        for($i=0; $i -lt $this.Rows.Count; $i++) {
            $ret += $this.Rows[$i].ToCsv() + "`r`n"
        }

        return $ret
    }

    [TableItem] Parse([object[]]$item) {
        $rws = $item | ?{$_.GetType().Name -eq 'TableRow' -or $_.GetType().Name -eq 'Row'}

        switch($rws.GetType().Name) {
            'Object[]' {
                foreach($r in $rws) {
                    $this.Rows += [RowItem]::new($this.Document, $this).Parse($r)
                }
                break;
            }
            {$_ -eq 'TableRow' -or $_ -eq 'Row'} {
                $this.Rows += [RowItem]::new($this.Document, $this).Parse($rws)
                break;
            }
        }

        return $this
    }
}

class StringItem : BaseItem {
    [string] $Text

    StringItem([DocumentItem] $Document, [ParagraphItem] $Parent) : base($Document, $Parent) {
    }

    [string] ToString() {
        return "$($this.Text)"
    }

    [string] ToMarkdown([bool] $GithubFlawor, [hashtable] $ListCounter) {
        $ret = $this.TextDecoration($GithubFlawor, $ListCounter)
        return $ret;
    }

    [string] SetHeading([string] $Text, [int] $Head) {
        if(!$this.Parent.OneTimeOp) {
            $this.Parent.OneTimeOp = $true
            return "$('#' * $Head) $Text";
        }
        return $Text
    }

    static [string] ToRomanNumber([int] $Number, [bool] $Lower)
    {
        $result = ''
        $digitsValues = @( 1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000 )
        $romanDigits = @( "I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M" )
        while ($Number -gt 0)
        {
            for ($i = $digitsValues.Count - 1; $i -ge 0; $i--) {
                if ($number / $digitsValues[$i] -ge 1) {
                    $number -= $digitsValues[$i];
                    $result += ($romanDigits[$i]);
                    break;
                }
            }
        }
        if($Lower) {$result = $result.ToLower();}
        return $result;
    }

    static [string] ToLetterNumber([byte] $Number, [bool] $Lower) {
        $result = '';
        if($Number -lt 28) {
            $array = @()
            for($i=97; $i -lt (97+28); $i++) { $array += [System.Text.Encoding]::ASCII.GetChars($i)}            
            $result += $array[$Number]
        }
        return $result;
    }

    [string] TextDecoration([bool] $GithubFlawor, [hashtable] $ListCounter) {
        # Return unmodified empty string or string with spaces only
        if([string]::IsNullOrEmpty($this.Text) -or [string]::IsNullOrEmpty($this.Text.Trim())) {return $this.Text}

        $ret = $this.Text;
        $lastSpace = ''; $firstSpace = '';
        if($ret.EndsWith(' ')) {$lastSpace = ' '; $ret = $ret.TrimEnd();}
        if($ret.StartsWith(' ')) {$firstSpace = ' '; $ret = $ret.TrimStart();}

        # Replace or Escape special characters
        $ret = $ret.Replace('|', ':')
        $ret = $ret.Replace('`', '\`').
               Replace('*', '\*').
               Replace('_', '\_').
               Replace('#', '\#').
               Replace('+', '\+').
               Replace('-', '\-').
               Replace('.', '\.').
               Replace('!', '\!').
               Replace('(', '\(').
               Replace(')', '\)').
               Replace('{', '\{').
               Replace('}', '\}').
               Replace('[', '\[').
               Replace(']', '\]').
               Replace('<', '\<').
               Replace('>', '\>')


        #Common
        if($this.Properties.Caps) {$ret = $ret.ToUpperInvariant()}
        if(![string]::IsNullOrEmpty($this.Properties.Link)) {$ret = "[$ret]($($this.Properties.Link))"}
        if($this.Properties.Bold) {$ret = "**$($ret)**"}
        if($this.Properties.Italic) {$ret = "*$($ret)*"}        
        if(![string]::IsNullOrEmpty($this.Properties.Color)) {
            if($this.Document.Cfg.Colors.UseColors) {
                try {
                    $color = [int32]"0x$($this.Properties.Color)"
                    $color = "#$($this.Properties.Color)"
                }
                catch{
                    $color = $this.Properties.Color
                }
                foreach($c in $this.Document.Cfg.Colors.Replace) {
                    if($color -eq $c.From) {$color = $c.To; break;}
                }
                $ret = "<span style=`"color:$color`">$($ret)</span>"
            }
        }

        #Github
        if($GithubFlawor) {
            if($this.Properties.Strike) {$ret = "~~$($ret)~~"}
            if($this.Properties.Highlight) {$ret = "<mark>$($ret)</mark>"}
            if($this.Properties.Underline) {$ret = "<u>$($ret)</u>"}
        }

        # Headings
        switch($this.Parent.Properties.Style) {
            {$this.Parent.Properties.Style -eq $this.Document.Cfg.Head.PSObject.Properties.Name[0]} {$ret = $this.SetHeading($ret, 1); break;}
            {$this.Parent.Properties.Style -in $this.Document.Cfg.Head.PSObject.Properties.Name[1]} {$ret = $this.SetHeading($ret, 2); break;}
            {$this.Parent.Properties.Style -in $this.Document.Cfg.Head.PSObject.Properties.Name[2]} {$ret = $this.SetHeading($ret, 3); break;}
            {$this.Parent.Properties.Style -in $this.Document.Cfg.Head.PSObject.Properties.Name[3]} {$ret = $this.SetHeading($ret, 4); break;}
            {$this.Parent.Properties.Style -in $this.Document.Cfg.Head.PSObject.Properties.Name[4]} {$ret = $this.SetHeading($ret, 5); break;}
            {$this.Parent.Properties.Style -in $this.Document.Cfg.Head.PSObject.Properties.Name[5]} {$ret = $this.SetHeading($ret, 6); break;}
        }
        
        #List        
        if($this.Parent.Properties.Style -eq 'List') {
            if(!$this.Parent.OneTimeOp) {
                $indent = '  ' * $this.Parent.Properties.ListLevel
                if($null -ne $this.Parent.Properties.ListNumbering -and $this.Parent.Properties.ListLevel -ge 0) {
                    $curr = ($this.Parent.Properties.ListNumbering | ?{$_.GetType().Name -eq 'Level'})
                    if($null -ne $curr) {$level = $curr[$this.Parent.Properties.ListLevel]} else {$level = 0;}
                }
                else {$level = 0;}
                $format = [BaseItem]::GetFromChainOfNames($level, 'NumberingFormat').Val.Value
                $value = ''; $currentNum = 1;
                if($this.Parent.Properties.ListLevel -ge 0) {
                    $currentNum = [BaseItem]::GetFromChainOfNames($level, 'StartNumberingValue').Val.Value + $ListCounter.($this.Parent.Properties.ListLevel)
                }
                if([string]::IsNullOrEmpty($format)) {$format = 'bullet'}
                switch($format) {
                    'decimal' {
                        $value = "$($currentNum)."
                        break;
                    }
                    'bullet' {
                        $value = '-'
                        break;
                    }
                    'lowerLetter' {
                        $value = "$([StringItem]::ToLetterNumber($currentNum, $true))."
                        break;
                    }
                    'lowerRoman' {
                        $value = "$([StringItem]::ToRomanNumber($currentNum, $true))."
                        break;
                    }
                }
                $ret = "$indent$value $ret"
            }
            $this.Parent.OneTimeOp = $true            
        }

        # Returning spaces back
        $ret = $firstSpace + $ret + $lastSpace;

        return $ret;
    }

    [StringItem] Parse($item, [string]$Hyperlink) {
        $tmp = $this.Parse([BaseItem]::GetFromChainOfNames($item, 'Run'))
        $this.Properties.Link = $Hyperlink
        return $this
    }

    [StringItem] Parse($item) {
        foreach($i in $item) {
            $Color = ''; $Highlight = ''
            switch($i.GetType().Name) {                
                'RunProperties' {
                    if($null -ne $i.Color -and $i.Color.Val -and $i.Color.Val.HasValue) {$Color = $i.Color.Val.Value}
                    if($null -ne $i.Highlight -and $i.Highlight.Val -and $i.Highlight.Val.HasValue) {$Highlight = $i.Highlight.Val.Value}
                    $this.Properties = @{
                        Bold = ($i | ?{$_.GetType().Name -eq 'Bold'} | Measure).Count -ge 1;
                        Italic = ($i | ?{$_.GetType().Name -eq 'Italic'} | Measure).Count -ge 1;
                        Strike = ($i | ?{$_.GetType().Name -eq 'Strike'} | Measure).Count -ge 1;
                        Caps = ($i | ?{$_.GetType().Name -eq 'Caps'} | Measure).Count -ge 1;
                        Color = $Color;
                        HighLight = $Highlight;
                        Underline =($i | ?{$_.GetType().Name -eq 'Underline'} | Measure).Count -ge 1;
                        Link = $null;
                    }
                    break;
                }
                'Text' {
                    $this.Text = $this.NormalizeText($i.Text)
                    break;
                }
            }
        }
        return $this;
    }

    [string] NormalizeText([string] $str) {
        $hex1 = 'E2 80 9C'; $h1 = '';
        $hex2 = 'E2 80 9D'; $h2 = '';

        $hex1.Split(" ") | forEach {[char]([convert]::toint16($_,16))} | forEach {$h1 += $_}
        $hex1.Split(" ") | forEach {[char]([convert]::toint16($_,16))} | forEach {$h2 += $_}
        # string below contains special chars. Do not touch them if you can't see them
        $ret = '';

        $str = ($str -replace $h1, '"') -replace $h2, '"'
        foreach($rep in $this.Document.Cfg.ReplaceThis) {
            $str = $str -Replace $rep.From, $rep.To
        }
        return $str;
    }
}

class ImgItem : BaseItem {
    [string] $ImageDir
    [string] $RelativeDir = 'images'

    ImgItem([DocumentItem] $Document, [BaseItem] $Parent) : base($Document, $Parent) {
        $this.ImageDir = Join-Path $this.Document.MdDir $this.RelativeDir
        if(!(Test-Path $this.ImageDir)) {md $this.ImageDir <#-Force -Confirm:$false#> | Out-Null} # - in Ubuntu those 2 options bring us to the error .NET Core 6.0.100, powershell v7.2.0
    }

    [string] ToString() {
        $ret = "<Image: [$(Join-Path $this.RelativeDir "$($this.Properties.ImageName)")]`r`n"
        $ret += "Image.Description [$($this.Properties.Descr)]`r`n"
        $ret += "Image.Title: [$($this.Properties.Title)]`r`n"
        $ret += "Image.Link [$($this.Properties.Link)]>"
        return $ret
    }

    [string] ToMarkdown([bool] $GithubFlawor, [hashtable] $ListCounter) {
        $prefix = ''; $ret = '';
        if($this.Document.Cfg.Image.UsePrefix) {
            if(![string]::IsNullOrEmpty($this.Document.Cfg.Image.Prefix)) {
                $prefix = $this.Document.Cfg.Image.Prefix
            }
        }

        if(![string]::IsNullOrEmpty($prefix)) {$imgUrl = Join-Path $prefix "$($this.RelativeDir)/$($this.Properties.ImageName)"}
        else {$imgUrl = "$($this.RelativeDir)/$($this.Properties.ImageName)"}
        # $imgUrl = [System.Web.HttpUtility]::UrlEncode($imgUrl)

        # Image
        if($this.Document.Cfg.Image.Embed) {
            $ext = [System.IO.Path]::GetExtension($this.Properties.ImageName).Trim('.')
            $imgUrl = "data:image/$ext;base64," + [convert]::ToBase64String([System.IO.File]::ReadAllBytes((Join-Path $($this.ImageDir) $($this.Properties.ImageName))))
        }
        if(![string]::IsNullOrEmpty($this.Properties.Title)) {$ret = "![$($this.Properties.Descr)]($imgUrl)`"$($this.Properties.Title)`"".Replace('\','/')}
        else {$ret = "![$($this.Properties.Descr)]($imgUrl)".Replace('\','/')}
        # Image Link
        if(![string]::IsNullOrEmpty($this.Properties.Link)) {$ret = "[$ret]($($this.Properties.Link))".Replace('\','/')}
        
        return $ret
    }

    [ImgItem] Parse($item) {
        if($item.GetType().Name -eq 'Picture') {
            $this.GetImgAndItsProperties($item, 'NonVisualPictureProperties.NonVisualDrawingProperties', 'BlipFill.Blip')
        }
        else {
            foreach($i in $item) {
                $Color = ''; $Highlight = ''
                switch($i.GetType().Name) {
                    'Drawing' {
                        $this.GetImgAndItsProperties($i, 'Inline.Graphic.GraphicData.Picture.NonVisualPictureProperties.NonVisualDrawingProperties', 'Inline.Graphic.GraphicData.Picture.BlipFill.Blip')
                        break;
                    }
                }
            }
        }
        return $this;
    }

    [void] GetImgAndItsProperties($ImgOrPicture, [string] $NonVisualDrawingChain, [string] $BlipChain) {
        $NonVisualDrawingProperties = [BaseItem]::GetFromChainOfNames($ImgOrPicture, $NonVisualDrawingChain)
        if($null -ne $NonVisualDrawingProperties) {
            $hyper = $null
            if($NonVisualDrawingProperties.HyperLinkOnClick -and $NonVisualDrawingProperties.HyperLinkOnClick.Id.HasValue) {
                $hyper = $this.Document.Document.MainDocumentPart.GetReferenceRelationship($NonVisualDrawingProperties.HyperLinkOnClick.Id.Value)
                $hyper = $hyper.Uri.ToString()
            }

            $tmp = '';
            if($null -ne $NonVisualDrawingProperties -and $null -ne $NonVisualDrawingProperties.Description) {
                $tmp = $NonVisualDrawingProperties.Description.ToString()
            }
            if(![string]::IsNullOrEmpty($tmp)) {
                $tmp = $tmp.Replace("`r", '').Replace("`n", '')
            }

            $q = '';
            if($null -ne $NonVisualDrawingProperties.Title) {$q = $NonVisualDrawingProperties.Title.ToString()}
            $this.Properties = @{
                Id = [guid]::NewGuid() #$NonVisualDrawingProperties.Id;
                Descr = $tmp;
                Title = $q
                Link = $hyper
                ImageName = $null
            }    
        }
        $blip = [BaseItem]::GetFromChainOfNames($ImgOrPicture, $BlipChain)
        if($null -ne $blip -and $null -ne $blip.Embed -and $blip.Embed.HasValue) {
            $img = $null
            switch($this.Document.Document.DocumentType) {
                'Document' {
                    $img = $this.Document.Document.MainDocumentPart.GetPartById($blip.Embed.Value);
                    $this.SaveImage($img)
                    break;
                }
                'Presentation' {
                    $xml = $this.Document.Document.PresentationPart.GetPartById($blip.Embed.Value);
                    $xml.ImageParts | %{
                        $this.SaveImage($_)
                    }
                    break;
                }
            }
            
        }
    }

    [System.Drawing.Imaging.ImageFormat] GetDotNetImageFormat($ext) {
        $ret = $null
        switch ($ext.TrimStart('.').ToLower())
        { 
            "bmp" {$ret = [System.Drawing.Imaging.ImageFormat]::Bmp; break;}
            "gif" {$ret = [System.Drawing.Imaging.ImageFormat]::Gif; break;}
            {$_ -eq "jpg" -or $_ -eq 'jpeg'} {$ret = [System.Drawing.Imaging.ImageFormat]::Jpeg; break}
            "ico" {$ret = [System.Drawing.Imaging.ImageFormat]::Icon; break;}
            "png" {$ret = [System.Drawing.Imaging.ImageFormat]::Png; break;}
            {$_ -eq "tif" -or $_ -eq 'tiff'} {$ret = [System.Drawing.Imaging.ImageFormat]::Tiff; break;}
            "emf" {$ret = [System.Drawing.Imaging.ImageFormat]::Emf; break;}
            "wmf" {$ret = [System.Drawing.Imaging.ImageFormat]::Wmf; break;}
            "exif" {$ret = [System.Drawing.Imaging.ImageFormat]::Exif; break;}
        }
        return $ret;
    }

    [System.Drawing.Bitmap] ResizeImageAccordingToCfgSettings([System.Drawing.Image] $image, $interpolationMode) {
        # it could be resized or returned as-is
        $retImage = $image;

        if($this.Document.Cfg.Image.Convert.Resize.UseResize) {
            # We must use just one of 2 dimension: MaxWidth or MaxHeight
            # MaxWidth is in priority
            $width = 100; $height = 100; $magnify = 1;

            if($this.Document.Cfg.Image.Convert.Resize.MaxWidth -gt 0) {
                if($image.Width -gt $this.Document.Cfg.Image.Convert.Resize.MaxWidth -or !$this.Document.Cfg.Image.Convert.Resize.DoNotResizeLessThanMax) {
                    $magnify = ($image.Width/$this.Document.Cfg.Image.Convert.Resize.MaxWidth)
                }
            }
            elseif($this.Document.Cfg.Image.Convert.Resize.MaxHeight) {
                if($image.Height -gt $this.Document.Cfg.Image.Convert.Resize.MaxHeight -or !$this.Document.Cfg.Image.Convert.Resize.DoNotResizeLessThanMax) {
                    $magnify = ($image.Height/$this.Document.Cfg.Image.Convert.Resize.MaxHeight)
                }
            }
            else {return $retImage}

            if($magnify -ne 1) {
                $width = [int]($image.Width / $magnify)
                $height = [int]($image.Height / $magnify)
                $retImage = [ImgItem]::ResizeImage($image, $width, $height, $interpolationMode)
            }
        }

        return $retImage;
    }

    static [System.Drawing.Bitmap] ResizeImage([System.Drawing.Image] $image, [int] $width, [int] $height, $interpolationMode) {
        $destRect = [System.Drawing.Rectangle]::new(0, 0, $width, $height);
        $destImage = [System.Drawing.Bitmap]::new($width, $height);

        $destImage.SetResolution($image.HorizontalResolution, $image.VerticalResolution);

        $graphics = [System.Drawing.Graphics]::FromImage($destImage)
        $graphics.CompositingMode = [System.Drawing.Drawing2D.CompositingMode]::SourceCopy;
        $graphics.CompositingQuality = [System.Drawing.Drawing2D.CompositingQuality]::HighQuality;
        $graphics.InterpolationMode = [System.Drawing.Drawing2D.InterpolationMode]::$interpolationMode; #HighQualityBilinear; #HighQualityBicubic;
        $graphics.SmoothingMode = [System.Drawing.Drawing2D.SmoothingMode]::HighQuality;
        $graphics.PixelOffsetMode = [System.Drawing.Drawing2D.PixelOffsetMode]::HighQuality;

        $wrapMode = [System.Drawing.Imaging.ImageAttributes]::new()
        $wrapMode.SetWrapMode([System.Drawing.Drawing2D.WrapMode]::TileFlipXY);
        $graphics.DrawImage($image, $destRect, 0, 0, $image.Width, $image.Height, [System.Drawing.GraphicsUnit]::Pixel, $wrapMode);

        return $destImage;
    }

    [bool] ConvertAndSave([DocumentFormat.OpenXml.Packaging.ImagePart]$img, [System.IO.Stream]$stream, [string]$savePath) {
        # Call Before Plugin
        if($this.Document.IsPluginPresent('ImgItem-BeforeConvertAndSave')) {
            $tmp = $this.Document.CallPlugin('ImgItem-BeforeConvertAndSave', @($img, $stream, $savePath, $this.Document.Cfg.Image))
            if($null -ne $tmp) {$img = $tmp[0]; $stream = $tmp[1]; $savePath = $tmp[2]}
        }

        # Call Replace Plugin (possible: resize & save as different image type)
        if($this.Document.IsPluginPresent('ImgItem-ReplaceConvertAndSave')) {return ($this.Document.CallPlugin('ImgItem-ReplaceConvertAndSave', @($img, $stream, $savePath, $this.Document.Cfg.Image)))}
        else {
            $ret = $false

            $extOrig = [System.IO.Path]::GetExtension($savePath)
            foreach($r in $this.Document.Cfg.Image.Convert.Replace) {
                if($null -ne $this.GetDotNetImageFormat($extOrig)) {
                    if(".$($r.From.TrimStart('.'))" -eq $extOrig) {
                        $format = $this.GetDotNetImageFormat($r.To)
                        if($null -ne $format) {
                            $extNew = $r.To
                            $path = Split-Path $savePath
                            $name = [System.IO.Path]::GetFileNameWithoutExtension($savePath)
                            $newImagePath = Join-Path $path "$name$extNew"
                            [System.Drawing.Bitmap]$imgStream = [System.Drawing.Image]::FromStream($stream)

                            # Call Resize Plugin
                            if($this.Document.IsPluginPresent('ImgItem-ResizeImage')) {
                                $imgStream = ($this.Document.CallPlugin('ImgItem-ResizeImage', @($imgStream, $this.Document.Cfg.Image)))[0]
                            }
                            else {$imgStream = $this.ResizeImageAccordingToCfgSettings($imgStream, $this.Document.Cfg.Image.Convert.Resize.InterpolationMode)}
                            
                            $imgStream.Save($newImagePath, $format)
                            $this.Properties.ImageName = [System.IO.Path]::GetFileName($newImagePath)
                            $ret = $true;
                        }
                        else {toLog "Such image format defined in configuration [$($r.To)] doesn't support. Please contact Developer" warning}
                        break;
                    }
                }
                else {toLog "Current file has [$extOrig] extension, but conversion files of such type is not supported. Please contact Developer" warning}
            }
            if(!$ret) {toLog "Image [$savePath] wasn't converted and left as-is" warning}
        }

        # Call After Plugin
        if($this.Document.IsPluginPresent('ImgItem-AfterConvertAndSave')) {
            $ret = $this.Document.CallPlugin('ImgItem-AfterConvertAndSave', @($ret, $this.Document.Cfg.Image))
        }

        return $ret
    }

    [void] SaveImageAsIs($img, $stream, $savePath) {
        $buffer = New-Object System.Byte[] $stream.length
        $stream.Read($buffer, 0, $stream.length);
        $fs = New-Object IO.FileStream(
            $savePath, 
            [System.IO.FileMode]::OpenOrCreate, 
            [System.IO.FileAccess]::Write, 
            [IO.FileShare]::None
        )
        $fs.Write($buffer, 0, $buffer.Length);
        $fs.Close();
        $fs.Dispose();
        $this.Properties.ImageName = [System.IO.Path]::GetFileName($savePath)
    }

    [void] SaveImage($img) {
        if($null -ne $img) {
            $stream = $img.GetStream();
            $savePath = (Join-Path $this.ImageDir "$($this.Properties.Id)$([System.IO.Path]::GetExtension($img.Uri))")
            if($this.Document.Cfg.Image.Convert.UseConversion) {
                $tmp = $this.ConvertAndSave($img, $stream, $savePath)
                if(!$tmp) {$this.SaveImageAsIs($img, $stream, $savePath)}
            }
            else {
                $this.SaveImageAsIs($img, $stream, $savePath)               
            }
        }
    }
}