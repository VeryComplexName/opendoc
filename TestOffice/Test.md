﻿
This is simple string

![LogoDescription automatically generated](images/db96b366-8ac8-4eb4-bc23-0f13ca72f08e.png)

This is complex string with **Bold text**, *Italic text*, ***Bold and Italic text***, ~~Strike through text~~, <u>Underline text</u> and <span style="color:#FF0000">Colored text</span>\.

<u>Below is Unordered list</u> <u>\(and this line is underscored\)</u><u>:</u>

- **Bold** and ~~Strikethrough~~ text
- *Italic* <span style="color:#538135">Colored</span> and <u>*Underline*</u> <u>*Italic*</u>
  - <span style="color:#C45911">**Different color and Bold**</span>

<u>Below</u> <u>O</u><u>rdered list \(and this line is underscored\)</u><u>:</u>

1. <span style="color:#FF0000">**Bold Red**</span>
2. ~~*Italic and Strikethrough*~~
  b. Simple text

<u>Below ordered list tagged by Roman symbols \(and this line is underscored\):</u>

i. Roman1
ii. <span style="color:#00B050">Roman2 Colored</span>



<u>Below ordered list tagged by Alpha symbols \(and this line is underscored\):</u>

b. <span style="color:#C00000">*Alpha Colored and Italic*</span>
c. Alpha2 Common



| Left aligned column                                                                                                                                                                      | Centered column                                                                                                                                       | Right aligned column                                                                        |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------: | ------------------------------------------------------------------------------------------: |
| <br>- <span style="color:#2F5496">Colored</span><br>- <span style="color:#2F5496">*Same color and Italic*</span><br>- Common **Bold** <span style="color:#BF8F00">Different color</span> | **Bold**                                                                                                                                              | 77676767                                                                                    |
| <u>~~***Bold Italic Underline***~~</u>                                                                                                                                                   | <mark>Common</mark> <mark>Highlighted</mark>below you can see the image with link                                                                     | <br>1. Common<br>2. <span style="color:#C9C9C9">*Grayed text*</span><br>3. ***Italic***     |
| ![ShapeDescription automatically generated with low confidence](images/e6b50829-5484-4663-ac60-6074ad16a8f9.png)                                                                         | [![IconDescription automatically generated](images/373f4a1e-5e04-4d97-9719-7f8ffd22365d.png)](https://guides.github.com/features/mastering-markdown/) | ![IconDescription automatically generated](images/72dd52e5-dd1b-4974-88c6-5600b03d456f.png) |






-----


This is a title

| 1    | 2    | 3    |
| ---- | ---- | ---- |
| dfd  | dfdf | dfd  |
| dfd  | dfd  | dfd  |

| www  | eee  | rrr  | ttt  |
| ---- | ---- | ---- | ---- |
|      |      |      |      |

Textbox

![](images/bcbf6993-8815-4d33-abb4-d2f95f8db23d.png)

![IconDescription automatically generated](images/86fdf7cd-6aa0-4859-a566-32a6c60acc3a.png)

Second slide title

| Title Left aligned                                                                            | Title Center aligned | Title Right Aligned                             |
| --------------------------------------------------------------------------------------------- | -------------------- | ----------------------------------------------- |
| qqq                                                                                           | rrrrrrr              | 66666666666                                     |
| W w w wwww www<br><br>Dfdf d fddf dfd<br><br>Dfdf df dfd<br><br>Gfgfgfgfgf<br><br>fgfgfgfgfgf | rrrrrrrrrrrrrrrrr    | Ggggg<br><br>Gfgfgffgfg<br><br>fgfhvnghjhjggggh |

![](images/49759c5a-4f3b-4e8a-afd1-ec2975cce4a2.png)


-----



### Sheet3


|      |              |      | 1    | 2    | 3    | 4    | 5    |
| ---- | ------------ | ---- | ---- | ---- | ---- | ---- | ---- |
|      |              |      | 6    | 7    | 8    | 9    | 10   |
|      |              |      | 11   | 12   | 13   | 14   | 15   |
|      |              |      |      |      |      |      |      |
| 4,35 |              |      |      |      |      |      |      |
|      |              |      |      |      |      |      |      |
|      | www          |      |      |      |      |      |      |
|      |              |      |      |      |      |      |      |
|      |              |      |      |      |      |      |      |
|      | 22\.11\.2021 |      |      |      |      |      |      |


### Sheet2


| 22   |              |       |
| ---- | ------------ | ----- |
|      | 22\.11\.2021 |       |
|      |              | 44\.6 |


### Sheet1


| 12\.11\.2021 | 2          | 3    |
| ------------ | ---------- | ---- |
|     4,00     | FALSE      | 6    |
| 789\.6789    | 394\.83945 | 9    |


-----

Using the LIFEBOOK Application Panel
Launching Applications with the LIFEBOOK Application Panel
The application panel enables you to launch applications with the touch of a button when your system is on. Pressing 
any of the buttons will launch a user-defined application. Your notebook is pre-installed with software utilities that 
let you operate and configure your LIFEBOOK Application Panel.
Configuring your LIFEBOOK Application Panel  
When you start Windows, the LIFEBOOK Application Panel is automatically activated. As an application launcher, the 
LIFEBOOK Application Panel is very flexible, giving you a variety of options. To set up the panel to best suit your needs, 
we have provided the Application Panel Setup utility that quickly and easily helps you make the most of this valuable 
feature.
To configure your LIFEBOOK Application Panel with Application Panel Setup:  
1 Right-click on the Start icon. Click All Apps.
2 Click on Setup of LIFEBOOK Application Panel. The Button Setting utility will appear. The tabs that appear in the 
utility window correspond to the application buttons on the LIFEBOOK Application Panel. When you receive your 
notebook, these buttons are pre-configured to launch specific programs.
To change an application associated with the Application buttons:  
1 Click on the tab for the button you would like to reconfigure – for example, Application A. 
2 Click on the Enable this button check box if it is unchecked, then click on the [Application Registration] button. 
3 Click the Start Program check box, then click [Next]
4 On the screen that appears, click Select from Apps and scroll through the list to select the application you want 
to start with the Application A button.
5 Click [Next] -> [Next] -> [Finish]. On the Button Setting window, click [Apply], then click [OK]. The button will now 
launch the new application.
1 - 

#### 1

The Internet tab is a little bit different. It comes set to launch your default Internet browser (Internet Explorer), unless 
you have changed it in Windows. To reconfigure it to launch another program, follow these steps:
1 Click on the Internet tab. 
2 Click on the Enable this button check box if it is unchecked, then click on the [Application Registration] button. 
3 Click the Start program check box, then click [Next].
4 On the screen that appears, click the Select from Apps check box and scroll through the list to select the 
application you want to start with the Internet button.
5 Click [Next] -> [Next] -> [Finish]. On the Button Setting window, click [Apply], then click [OK]. 
The button will now launch the new application. If you want to return to launching your Windows default Internet 
browser with this button, click on the Start Browser check box instead of the Start program check box. Be aware that 
you will erase the settings for the “other application”. If you wish to go back to launching the “other application” from 
this button, you will need to reconfigure it as described above.
When you have finished with button settings, click [OK], and the new settings will take effect. You can reconfigure 
your LIFEBOOK Application Panel as often as you like. 
IF ONE OF THE APPLICATION LAUNCHER BUTTONS IS IDENTIFIED AS AN INTERNET LAUNCHER, THE BUTTON CAN STILL BE CONFIGURED TO LAUNCH 
ANY APPLICATION YOU WISH; IT IS NOT LIMITED TO BEING AN INTERNET BROWSER.
2 - 

#### 2



-----


